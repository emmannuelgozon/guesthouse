package co.guesthouse.project.data.network;

import com.google.gson.JsonArray;

import co.guesthouse.project.model.CheckOutResponse;
import co.guesthouse.project.model.SearchVacationResponse;
import co.guesthouse.project.model.SpecificResponse;
import co.guesthouse.project.model.check_reservation_dates.ReservationDatesResponse;
import co.guesthouse.project.model.checkout_confirm.CheckOutConfirmResponse;
import retrofit2.Call;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface APIRetrofitInterface {

    @POST("/api/search")
    Call<SearchVacationResponse> doPostServerGetListings(
            @Query("token") String token,
            @Query("check_in") String checkIn,
            @Query("check_out") String checkOut,
            @Query("guest") String guest);


    @POST("/api/specific")
    Call<SpecificResponse> doPostServerGetSpecific(
            @Query("token") String token,
            @Query("permalink") String permalink
    );


    @POST("/api/available-dates")
    Call<JsonArray> doPostServerGetAvailableDates(
            @Query("token") String token,
            @Query("permalink") String permalink,
            @Query("year") String year,
            @Query("month") String month
    );

    @POST("api/compute-amount")
    Call<JsonArray> doPostServerGetComputeAmount(
            @Query("token") String token,
            @Query("permalink") String permalink,
            @Query("check_in") String checkIn,
            @Query("check_out") String checkOut

    );

    @POST("api/check-reservation-dates")
    Call<ReservationDatesResponse> doPostServerCheckReservationDates(
            @Query("token") String token,
            @Query("permalink") String permalink,
            @Query("check_in") String checkIn,
            @Query("check_out") String checkOut

    );


    @POST("api/checkout")
    Call<CheckOutResponse> doPostServerCheckout(
            @Query("token") String token,
            @Query("permalink") String permalink,
            @Query("check_in") String checkIn,
            @Query("check_out") String checkOut,
            @Query("first_name") String firstName,
            @Query("last_name") String lastName,
            @Query("email") String email,
            @Query("mobile_number") String mobileNumber,
            @Query("country") String country,
            @Query("address") String address,
            @Query("zip_code") String zipCode,
            @Query("credit_card") String creditCard,
            @Query("cvc") String cvc,
            @Query("exp_month") String expiryMonth,
            @Query("exp_year") String expiryYear
    );


    @POST("api/checkout_confirm")
    Call<CheckOutConfirmResponse> doPostServerCheckoutConfirm(
            @Query("token") String token,
            @Query("permalink") String permalink,
            @Query("check_in") String checkIn,
            @Query("check_out") String checkOut,
            @Query("first_name") String firstName,
            @Query("last_name") String lastName,
            @Query("email") String email,
            @Query("mobile_number") String mobileNumber,
            @Query("country") String country,
            @Query("address") String address,
            @Query("zip_code") String zipCode,
            @Query("credit_card") String creditCard,
            @Query("cvc") String cvc,
            @Query("exp_month") String expiryMonth,
            @Query("exp_year") String expiryYear
    );

}