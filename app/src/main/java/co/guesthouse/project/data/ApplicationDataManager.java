package co.guesthouse.project.data;

import android.content.Context;

import com.google.gson.JsonArray;

import javax.inject.Inject;

import co.guesthouse.project.data.network.ApiHelper;
import co.guesthouse.project.data.network.ApiHelperInterface;
import co.guesthouse.project.model.CheckOutModel;
import co.guesthouse.project.model.CheckOutResponse;
import co.guesthouse.project.model.SearchVacationResponse;
import co.guesthouse.project.model.SpecificResponse;
import co.guesthouse.project.model.check_reservation_dates.ReservationDatesResponse;
import co.guesthouse.project.model.checkout_confirm.CheckOutConfirmResponse;
import retrofit2.Call;


public class ApplicationDataManager implements ApiHelperInterface {

    private static final String TAG = "ApplicationDataManager";

    private final Context mContext;
    private final ApiHelper mApiHelper;


    @Inject
    ApplicationDataManager(Context context, ApiHelper apiHelper){
        mContext = context;
        mApiHelper = apiHelper;
    }


    @Override
    public Call<SearchVacationResponse> doPostServerGetListings(String checkIn, String checkout, String numberOfGuests) {
        return mApiHelper.doPostServerGetListings(checkIn, checkout, numberOfGuests);

    }

    @Override
    public Call<SpecificResponse> doPostServerGetSpecific(String permalink) {
        return mApiHelper.doPostServerGetSpecific(permalink);
    }

    @Override
    public Call<JsonArray> doPostServerGetSpecificAvailableDates(String permalink, String year, String month) {
        return mApiHelper.doPostServerGetSpecificAvailableDates(permalink, year, month);
    }

    @Override
    public Call<JsonArray> doPostServerGetComputeAmount(String permalink, String checkIn, String checkout) {
        return mApiHelper.doPostServerGetComputeAmount(permalink, checkIn,checkout);
    }

    @Override
    public Call<ReservationDatesResponse> doPostServerCheckReservationDates(String permalink, String checkIn, String checkout) {
        return mApiHelper.doPostServerCheckReservationDates(permalink, checkIn, checkout);
    }

    @Override
    public Call<CheckOutResponse> doPostServerCheckout(CheckOutModel model){

    return mApiHelper.doPostServerCheckout(model);
    }

    @Override
    public Call<CheckOutConfirmResponse> doPostServerCheckoutConfirm(CheckOutModel model){

        return mApiHelper.doPostServerCheckoutConfirm(model);
    }
}
