package co.guesthouse.project.data.network;

import com.google.gson.JsonArray;

import javax.inject.Inject;

import co.guesthouse.BuildConfig;
import co.guesthouse.project.model.CheckOutModel;
import co.guesthouse.project.model.CheckOutResponse;
import co.guesthouse.project.model.SearchVacationResponse;
import co.guesthouse.project.model.SpecificResponse;
import co.guesthouse.project.model.check_reservation_dates.ReservationDatesResponse;
import co.guesthouse.project.model.checkout_confirm.CheckOutConfirmResponse;
import retrofit2.Call;

public class ApiHelper implements ApiHelperInterface {

    private static final String TAG = "ApiHelper";

    private APIRetrofitInterface apiService;

    @Inject
    public ApiHelper(){
        apiService = APIClient.getClient().create(APIRetrofitInterface.class);
    }


    @Override
    public Call<SearchVacationResponse> doPostServerGetListings(String checkIn, String checkout,
                                                                String numberOfGuests) {

        return apiService.doPostServerGetListings(
                BuildConfig.REQUEST_TOKEN, checkIn, checkout,numberOfGuests);
    }

    @Override
    public Call<SpecificResponse> doPostServerGetSpecific(String permalink) {

        return apiService.doPostServerGetSpecific(
                BuildConfig.REQUEST_TOKEN, permalink
        );
    }

    @Override
    public Call<JsonArray> doPostServerGetSpecificAvailableDates(String permalink, String year, String month) {
        return apiService.doPostServerGetAvailableDates(
          BuildConfig.REQUEST_TOKEN,
          permalink,
          year,
          month

        );
    }

    @Override
    public Call<JsonArray> doPostServerGetComputeAmount(String permalink, String checkIn, String checkout) {
        return apiService.doPostServerGetComputeAmount(
                BuildConfig.REQUEST_TOKEN,
                permalink,
                checkIn,
                checkout
        );
    }

    @Override
    public Call<ReservationDatesResponse> doPostServerCheckReservationDates(String permalink, String checkIn, String checkout) {
        return apiService.doPostServerCheckReservationDates(
                BuildConfig.REQUEST_TOKEN,
                permalink,
                checkIn,
                checkout
        );
    }

    @Override
    public Call<CheckOutResponse> doPostServerCheckout(CheckOutModel model) {
        return apiService.doPostServerCheckout(
                BuildConfig.REQUEST_TOKEN,
                model.getPermalink(),
                model.getCheckIn(),
                model.getCheckOut(),
                model.getFirstName(),
                model.getLastName(),
                model.getEmail(),
                model.getMobileNumber(),
                model.getCountry(),
                model.getAddress(),
                model.getZipCode(),
                model.getCreditCard(),
                model.getCvc(),
                model.getExpMonth(),
                model.getExpYear()
        );
    }

    @Override
    public Call<CheckOutConfirmResponse> doPostServerCheckoutConfirm(CheckOutModel model) {
        return apiService.doPostServerCheckoutConfirm(
                BuildConfig.REQUEST_TOKEN,
                model.getPermalink(),
                model.getCheckIn(),
                model.getCheckOut(),
                model.getFirstName(),
                model.getLastName(),
                model.getEmail(),
                model.getMobileNumber(),
                model.getCountry(),
                model.getAddress(),
                model.getZipCode(),
                model.getCreditCard(),
                model.getCvc(),
                model.getExpMonth(),
                model.getExpYear()
        );
    }
}
