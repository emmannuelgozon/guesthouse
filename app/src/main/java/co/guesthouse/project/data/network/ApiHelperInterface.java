package co.guesthouse.project.data.network;

import com.google.gson.JsonArray;

import co.guesthouse.project.model.CheckOutModel;
import co.guesthouse.project.model.CheckOutResponse;
import co.guesthouse.project.model.SearchVacationResponse;
import co.guesthouse.project.model.SpecificResponse;
import co.guesthouse.project.model.check_reservation_dates.ReservationDatesResponse;
import co.guesthouse.project.model.checkout_confirm.CheckOutConfirmResponse;
import retrofit2.Call;
import retrofit2.http.Query;

public interface ApiHelperInterface {

    Call<SearchVacationResponse> doPostServerGetListings(
                                                         String checkIn,
                                                         String checkout,
                                                         String numberOfGuests);

    Call<SpecificResponse> doPostServerGetSpecific(
            String permalink
    );

    Call<JsonArray> doPostServerGetSpecificAvailableDates(

            String permalink,
            String year,
            String month
    );


    Call<JsonArray> doPostServerGetComputeAmount(
            String permalink,
            String checkIn,
            String checkout
    );

    Call<ReservationDatesResponse> doPostServerCheckReservationDates(
            String permalink,
            String checkIn,
            String checkout
    );

    Call<CheckOutResponse> doPostServerCheckout(
            CheckOutModel model
    );

    Call<CheckOutConfirmResponse> doPostServerCheckoutConfirm(
            CheckOutModel model
    );
}
