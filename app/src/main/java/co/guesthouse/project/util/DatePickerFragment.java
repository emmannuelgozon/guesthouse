package co.guesthouse.project.util;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.DatePicker;

import java.util.Calendar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import co.guesthouse.R;
import co.guesthouse.project.main.search_view.VacationHouseSearchFragment;

import static android.app.Activity.RESULT_OK;

public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener{

    private static final String TAG = "DatePickerFragment";

    VacationHouseSearchFragment fragment;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if(context != null){

        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        final Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog= new DatePickerDialog(getActivity(), R.style.MyDatePickerDialogTheme, this, year, month, day);
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());

        Dialog dialog = datePickerDialog;
        return dialog;



    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

        Log.d(TAG, "onDateSet: " + year + " " + month + " " +dayOfMonth);

        month += 1;

        String monthStr, dayStr;
        if(month < 10)  monthStr = "0" + month;
        else monthStr = Integer.toString(month);

        if(dayOfMonth < 10) dayStr = "0" + dayOfMonth;
        else dayStr = Integer.toString(dayOfMonth);

        sendResult(year + "", monthStr, dayStr);
    }


    private void sendResult( String year, String month, String dayOfMonth){

        Intent intent = new Intent();
        intent.putExtra("year", year);
        intent.putExtra("month", month);
        intent.putExtra("day", dayOfMonth);

        getTargetFragment().onActivityResult(getTargetRequestCode(), RESULT_OK, intent);

    }
}
