package co.guesthouse.project.main.check_out_view;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.Calendar;
import java.util.List;

import javax.inject.Inject;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.appcompat.widget.Toolbar;
import butterknife.BindView;
import butterknife.ButterKnife;
import co.guesthouse.R;
import co.guesthouse.project.base.BaseActivity;
import co.guesthouse.project.constant.AppConstants;
import co.guesthouse.project.model.CheckOutModel;
import co.guesthouse.project.model.ComputeAmountItems;
import co.guesthouse.project.model.ComputeAmountProcessFee;
import co.guesthouse.project.model.ComputeAmountResponse;
import co.guesthouse.project.model.ComputeAmountTotal;

public class CheckOutActivity extends BaseActivity implements CheckOutMvp.View{

    private static final String TAG = "CheckOutActivity";

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    /******************  Personal Information ***************/

    @BindView(R.id.first_name_tv)
    AppCompatEditText firstnameTv;

    @BindView(R.id.last_name_tv)
    AppCompatEditText lastNameTv;

    @BindView(R.id.email_tv)
    AppCompatEditText emailTv;

    @BindView(R.id.phone_number_tv)
    AppCompatEditText phoneNumberTv;

    @BindView(R.id.continue_to_use_email_cb)
    AppCompatCheckBox continueToUseEmailCb;

    /******************  Billing Address ***************/

    @BindView(R.id.country_spinner)
    AppCompatSpinner countrySpinner;

    @BindView(R.id.address_tv)
    AppCompatEditText addressTv;

    @BindView(R.id.zip_code_tv)
    AppCompatEditText zipCodeTv;

    /******************  Payment Details ***************/

    @BindView(R.id.card_number_tv)
    AppCompatEditText cardNumberTv;

    @BindView(R.id.cvc_tv)
    AppCompatEditText cvcTv;


    /******************  Expiration Date ***************/

    @BindView(R.id.expiry_month_spinner)
    AppCompatSpinner expiryMonthSpinner;

    @BindView(R.id.expiry_year_spinner)
    AppCompatSpinner expiryYearSpinner;


    /******************  Summary Layout ***************/


    @BindView(R.id.checkin_date_value_tv)
    TextView checkInDateValueTv;

    @BindView(R.id.checkout_date_value_tv)
    TextView checkOutDateValueTv;

    @BindView(R.id.price_desc_tv)
    TextView priceDescTv;

    @BindView(R.id.price_value_tv)
    TextView priceValueTv;

    @BindView(R.id.service_fee_desc_tv)
    TextView serviceFeeDescTv;

    @BindView(R.id.service_fee_value_tv)
    TextView serviceFeeValueTv;

    @BindView(R.id.total_desc_tv)
    TextView totalDescTv;

    @BindView(R.id.total_value_tv)
    TextView totalValueTv;

    /************* Book Now Button **************/
    @BindView(R.id.book_now_btn)
    Button bookNowBtn;


    @Inject
    CheckOutMvp.Presenter<CheckOutMvp.View> mPresenter;


    private ComputeAmountResponse mComputeAmountResponse;
    private String mPermalink, checkIn, checkOut;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.checkout_activity);

        getActivityComponent().inject(this);
        ButterKnife.bind(this);
        mPresenter.onAttach(this);

        initialize();
    }

    @Override
    protected void initialize() {

        Intent intent = getIntent();

        TextView titleTv = toolbar.findViewById(R.id.toolbar_title);
        titleTv.setText(getResources().getString(R.string.checkout_header_title));

        /*********** getting data from Booking Dialog ***********/


        if(intent.hasExtra("price_summary")){
            mComputeAmountResponse = intent.getParcelableExtra("price_summary");
            Log.d(TAG, "initialize: " + mComputeAmountResponse.toString());
            initializeBookingSummary();
        }

        if(intent.hasExtra("permalink")){
            mPermalink = intent.getStringExtra("permalink");
            Log.d(TAG, "initialize: " + mPermalink);
        }

        if(intent.hasExtra("check_in")){
            checkIn = intent.getStringExtra("check_in");
            checkInDateValueTv.setText(checkIn);
        }

        if(intent.hasExtra("check_out")){
            checkOut = intent.getStringExtra("check_out");
            checkOutDateValueTv.setText(checkOut);
        }


        initializeSpinners();
//        initializeSampleDate();

        bookNowBtn.setOnClickListener(v -> {
            //TODO: handle book now btn

            String firstName = firstnameTv.getText().toString();
            String lastName = lastNameTv.getText().toString();
            String email = emailTv.getText().toString();
            String phoneNumber = phoneNumberTv.getText().toString();

            String country = countrySpinner.getSelectedItem().toString();
            String address = addressTv.getText().toString();
            String zipCode = zipCodeTv.getText().toString();

            String cardNumber = cardNumberTv.getText().toString();
            String cvc = cvcTv.getText().toString();

            String expiryMonth;

            String x = expiryMonthSpinner.getSelectedItem().toString();
            Log.d(TAG, "initialize: x: " + x);
            Integer month = AppConstants.ENUM_MONTH_LONG1.valueOf(x).getValue();

            if(month < 10){
                expiryMonth = "0" + month;
            }
            else{
                expiryMonth = month + "";
            }

            String expiryYear = expiryYearSpinner.getSelectedItem().toString();

            CheckOutModel checkoutModel = new CheckOutModel();
            checkoutModel.setPermalink(mPermalink);
            checkoutModel.setCheckIn(checkIn);
            checkoutModel.setCheckOut(checkOut);
            checkoutModel.setFirstName(firstName);
            checkoutModel.setLastName(lastName);
            checkoutModel.setEmail(email);
            checkoutModel.setMobileNumber(phoneNumber);
            checkoutModel.setCountry(country);
            checkoutModel.setAddress(address);
            checkoutModel.setZipCode(zipCode);
            checkoutModel.setCreditCard(cardNumber);
            checkoutModel.setCvc(cvc);
            checkoutModel.setExpMonth(expiryMonth);
            checkoutModel.setExpYear(expiryYear);


            Log.d(TAG, "initialize:  permalink: " + mPermalink);
            Log.d(TAG, "initialize: check in: " + checkIn);
            Log.d(TAG, "initialize: check out: " + checkOut);

            Log.d(TAG, "initialize: first name: " + firstName);
            Log.d(TAG, "initialize: last name: " + lastName);
            Log.d(TAG, "initialize: email: " + email);
            Log.d(TAG, "initialize: phone number: " + phoneNumber);

            Log.d(TAG, "initialize: country: " + country);
            Log.d(TAG, "initialize: address: " + address);
            Log.d(TAG, "initialize: zip code: " + zipCode);

            Log.d(TAG, "initialize: card number: " + cardNumber);
            Log.d(TAG, "initialize: cvc: " + cvc);

            Log.d(TAG, "initialize: expiry month: " + expiryMonth);
            Log.d(TAG, "initialize: expiry year: " + expiryYear);


            mPresenter.checkOut(checkoutModel);

        });


    }

    private void initializeSampleDate() {
        firstnameTv.setText("sample first name");
        lastNameTv.setText("sample last name");
        emailTv.setText("sampleemail@gmail.com");
        phoneNumberTv.setText("639151234567");

        addressTv.setText("Nevada, Texas");
        zipCodeTv.setText("1234");

        cardNumberTv.setText("4242424242424242");
        cvcTv.setText("234");

    }


    private void initializeSpinners(){


        // country spinner
        ArrayAdapter<AppConstants.ENUM_COUNTRY> countryAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item,
                AppConstants.ENUM_COUNTRY.values());
        countrySpinner.setAdapter(countryAdapter);


        //expiration month
        String[] monthValues = new String[AppConstants.ENUM_MONTH_LONG1.values().length];

        int counter = 0;
        for(AppConstants.ENUM_MONTH_LONG1 month: AppConstants.ENUM_MONTH_LONG1.values()){
            monthValues[counter++] = month.name();
        }

        ArrayAdapter<String> monthAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item,
                monthValues);
        expiryMonthSpinner.setAdapter(monthAdapter);


        // expiration year
        Integer year = Calendar.getInstance().get(Calendar.YEAR);
        Integer[] yearValues = new Integer[11];

        for(int i = 0; i < 11; i++){
            yearValues[i] = year + i;
        }

        ArrayAdapter<Integer> yearAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item,
                yearValues);
        expiryYearSpinner.setAdapter(yearAdapter);


    }


    private void initializeBookingSummary(){


        List<ComputeAmountItems> item = mComputeAmountResponse.getItems();
        String itemPriceStr = new String();
        String itemNightsStr = new String();
        String itemSubtotalStr = new String();

        for(ComputeAmountItems itemArray: item){
            itemPriceStr = Integer.toString(itemArray.getPrice());
            itemNightsStr = Integer.toString(itemArray.getNights());
            itemSubtotalStr = Integer.toString(itemArray.getSubtotal());
        }
        ComputeAmountProcessFee amountProcessFee = mComputeAmountResponse.getProcessFee();

        String processFeePercentStr = amountProcessFee.getProcessFee();
        String processFeeSubtotal = amountProcessFee.getSubtotal();

        ComputeAmountTotal total = mComputeAmountResponse.getTotal();

        String totalStr = total.getTotal();

        priceDescTv.setText(getResources().getString(R.string.sgd_currency)
                + " "
                + itemPriceStr+ " "
                + "x" + " "
                + itemNightsStr + " "
                + getResources().getString(R.string.nights));

        priceValueTv.setText(getResources().getString(R.string.sgd_currency)
                + " "
                + itemSubtotalStr);

        serviceFeeDescTv.setText(getResources().getString(R.string.service_fee)
                + " ("
                + processFeePercentStr
                + ")");

        serviceFeeValueTv.setText(getResources().getString(R.string.sgd_currency)
                + " "
                + processFeeSubtotal);


        totalValueTv.setText(getResources().getString(R.string.sgd_currency)
                + " "
                + totalStr);
    }

    @Override
    public void onCheckOutSuccess() {

    }

    @Override
    public void onCheckOutFailure() {

    }
}
