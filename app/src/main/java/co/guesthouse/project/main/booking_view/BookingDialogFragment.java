package co.guesthouse.project.main.booking_view;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.timessquare.CalendarPickerView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatSpinner;
import butterknife.BindView;
import butterknife.ButterKnife;
import co.guesthouse.R;
import co.guesthouse.project.base.BaseDialogFragment;
import co.guesthouse.project.constant.AppConstants;
import co.guesthouse.project.dependency_injection.ActivityComponent;
import co.guesthouse.project.main.specific_view.SpecificListingActivity;
import co.guesthouse.project.model.ComputeAmountItems;
import co.guesthouse.project.model.ComputeAmountProcessFee;
import co.guesthouse.project.model.ComputeAmountResponse;
import co.guesthouse.project.model.ComputeAmountTotal;


public class BookingDialogFragment extends BaseDialogFragment {

    private static final String TAG = "BookingDialogFragment";

    @BindView(R.id.booking_calendar)
    CalendarPickerView calendarPickerView;

    @BindView(R.id.go_to_prev_month_btn)
    Button prevBtn;

    @BindView(R.id.go_to_next_month_btn)
    Button nextBtn;

    @BindView(R.id.month_spinner)
    AppCompatSpinner monthSpinner;

    @BindView(R.id.year_spinner)
    AppCompatSpinner yearSpinner;

    @BindView(R.id.checkin_checkout_date_layout)
    RelativeLayout checkinCheckoutLayout;

    @BindView(R.id.checkin_date_value_tv)
    TextView checkInDateValueTv;

    @BindView(R.id.checkout_date_value_tv)
    TextView checkOutDateValueTv;

    @BindView(R.id.price_desc_tv)
    TextView priceDescTv;

    @BindView(R.id.price_value_tv)
    TextView priceValueTv;

    @BindView(R.id.service_fee_desc_tv)
    TextView serviceFeeDescTv;

    @BindView(R.id.service_fee_value_tv)
    TextView serviceFeeValueTv;

    @BindView(R.id.total_value_tv)
    TextView totalValueTv;

    @BindView(R.id.book_now_btn)
    Button bookNowBtn;


    private Context context;
    private SpecificListingActivity activity;

    private Date currentDate;

    // current month displayed
    private Date displayedDate;

    List<Date> mDisabledDates = new ArrayList<>();
    List<Date> mSelectedDates = new ArrayList<>();

    Date mSelectedStartDate;
    Date mSelectedEndDate;



    private int onDateSelectCounter = -1;

    public static BookingDialogFragment newInstance(){
        BookingDialogFragment frag = new BookingDialogFragment();
        Bundle args = new Bundle();
        frag.setArguments(args);
        return frag;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;

        if(context instanceof SpecificListingActivity){
            this.activity = (SpecificListingActivity) context;
        }
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        return inflater.inflate(R.layout.booking_dialog_fragment_layout, container, false);
    }



    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        ActivityComponent component = getActivityComponent();
        if(component != null){
            ButterKnife.bind(this, view);
            getActivityComponent().inject(this);
        }
        initialize();

    }


    @Override
    protected void initialize() {

        currentDate = displayedDate = new Date();
        initializeSpinner();
        initializeCalendar();
        initializeCalendarNavigation();
        initializeButton();
    }

    private void initializeButton() {

        bookNowBtn.setOnClickListener(v -> {
            activity.checkReservationDates();
        });
    }


    // TODO: finish spinner logic
    private void initializeSpinner(){

        ArrayAdapter<AppConstants.ENUM_MONTHS_SHORT> monthAdapter = new ArrayAdapter<>(context,
                android.R.layout.simple_spinner_item, AppConstants.ENUM_MONTHS_SHORT.values());
        monthSpinner.setAdapter(monthAdapter);

        int year = currentDate.getYear() + 1900;
        int size = 2030 - year;
        Integer[] yearArrayList = new Integer[size];

        for(int i = 0; i < size; i++){
            yearArrayList[i] = year + i;
        }


        ArrayAdapter<Integer> yearAdapter = new ArrayAdapter<>(context,
                android.R.layout.simple_spinner_dropdown_item, yearArrayList);

        yearSpinner.setAdapter(yearAdapter);

    }



    // initialize calendar
    private void initializeCalendar(){

        Date availableDate = getAvailableDate(AppConstants.NO_OPERATION);
        activity.getAvailableDateFromServer(1900 + availableDate.getYear(), availableDate.getMonth(), true);

    }



    // setting calendarpickerview values after calling get available dates api
    public void setCalendar(List<Date> disabledDays,  int year, int month){

        int newMonth = month - 1;

        // setting displayedDate variable
        setViewedDate(year, newMonth);

        //setting visibility of calendar to Visible
        if(calendarPickerView.getVisibility() == View.GONE){
            calendarPickerView.setVisibility(View.VISIBLE);

        }

        //setting disabled dates
        if(disabledDays.size() > 0){

            // adding disabled dates to the list
            for(Date disabledDaysIndex : disabledDays){
                if(!mDisabledDates.contains(disabledDaysIndex)){

                    Log.d(TAG, "setCalendar: added date to disable: " + disabledDaysIndex.toString());
                    mDisabledDates.add(disabledDaysIndex);
                }
            }

            calendarPickerView.setDateSelectableFilter(date -> {

                for(Date index: mDisabledDates){
                    if(date.compareTo(index) == 0){
                        return false;
                    }
                }
                return true;
            });
        }


        calendarPickerView.setOnDateSelectedListener(new CalendarPickerView.OnDateSelectedListener() {

            @Override
            public void onDateSelected(Date date) {

                onDateSelectCounter ++;

                // N % 2 == 0 start date
                if(onDateSelectCounter % 2 == 0){
                    mSelectedStartDate = mSelectedEndDate = date;
                }

                // N % 2 == 1 end date
                else{

                    if(date.compareTo(mSelectedStartDate) <= 0){
                        mSelectedStartDate = mSelectedEndDate = date;
                        onDateSelectCounter --;
                    }
                    else{
                        mSelectedEndDate = date;
                    }
                }

                mSelectedDates = new ArrayList<>();
                Log.d(TAG, "onDateSelected: date list cleared");
                Log.d(TAG, "onDateSelected: start date: " + mSelectedStartDate.toString() + " end date: " + mSelectedEndDate.toString());
                addSelectedDates();
                callComputAmountAPI();

//                setCheckInCheckoutView();

            }

            @Override
            public void onDateUnselected(Date date) {

            }
        });


        List<Date> selectedDateForMonth = new ArrayList<>();


        if((newMonth == currentDate.getMonth()) && (year == (currentDate.getYear() + 1900))){
            if(prevBtn.isEnabled()){
                prevBtn.setEnabled(false);
            }
            calendarPickerView.init(getActualMinimum(), getActualMaximum())
                    .inMode(CalendarPickerView.SelectionMode.RANGE);
        }
        else{
            if(!prevBtn.isEnabled()){
                prevBtn.setEnabled(true);
            }
            calendarPickerView.init(getActualMinimum(), getActualMaximum())
                    .inMode(CalendarPickerView.SelectionMode.RANGE);
        }

        for(Date index: mSelectedDates){
            if(displayedDate.getMonth() == index.getMonth()){
                Log.d(TAG, "setCalendar: displayed date: " + displayedDate.toString() + " index: " + index.toString());
                Log.d(TAG, "setCalendar: displayed month: " + displayedDate.getMonth() + " index month: " + index.getMonth());
                selectedDateForMonth.add(index);
            }
        }

        calendarPickerView.highlightDates(selectedDateForMonth);

    }


    public void setBookingDetails(ComputeAmountResponse response){

        setCheckInCheckoutView();

        List<ComputeAmountItems> item = response.getItems();
        String itemPriceStr = new String();
        String itemNightsStr = new String();
        String itemSubtotalStr = new String();

        for(ComputeAmountItems itemArray: item){
            itemPriceStr = Integer.toString(itemArray.getPrice());
            itemNightsStr = Integer.toString(itemArray.getNights());
            itemSubtotalStr = Integer.toString(itemArray.getSubtotal());
        }
        ComputeAmountProcessFee amountProcessFee = response.getProcessFee();

        String processFeePercentStr = amountProcessFee.getProcessFee();
        String processFeeSubtotal = amountProcessFee.getSubtotal();

        ComputeAmountTotal total = response.getTotal();

        String totalStr = total.getTotal();

        priceDescTv.setText(getResources().getString(R.string.sgd_currency)
        + " "
        + itemPriceStr+ " "
        + "x" + " "
        + itemNightsStr + " "
        + getResources().getString(R.string.nights));

        priceValueTv.setText(getResources().getString(R.string.sgd_currency)
        + " "
        + itemSubtotalStr);

        serviceFeeDescTv.setText(getResources().getString(R.string.service_fee)
        + " ("
        + processFeePercentStr
        + ")");

        serviceFeeValueTv.setText(getResources().getString(R.string.sgd_currency)
        + " "
        + processFeeSubtotal);


        totalValueTv.setText(getResources().getString(R.string.sgd_currency)
        + " "
        + totalStr);




    }




    // setting displayDate values
    private void setViewedDate(int year, int month){
        // setting displayedDate
        Calendar viewedDateCalendar = Calendar.getInstance();
        viewedDateCalendar.set(Calendar.YEAR, year);
        viewedDateCalendar.set(Calendar.MONTH, month);
        viewedDateCalendar.set(Calendar.DAY_OF_MONTH, viewedDateCalendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        viewedDateCalendar.set(Calendar.HOUR_OF_DAY, 0);
        viewedDateCalendar.set(Calendar.MINUTE, 0);
        viewedDateCalendar.set(Calendar.SECOND, 0);
        viewedDateCalendar.set(Calendar.MILLISECOND, 0);
        displayedDate = viewedDateCalendar.getTime();
    }



    private Date getAvailableDate(byte comparison){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(displayedDate);

        switch(comparison){
            case AppConstants.ADD:
                calendar.add(Calendar.MONTH, 1);
                break;
            case AppConstants.SUBTRACT:
                calendar.add(Calendar.MONTH, -1);
                break;
            case AppConstants.NO_OPERATION:
            default:
                break;
        }
        return calendar.getTime();
    }


    private Date getActualMinimum(){
        if(currentDate.getMonth() == displayedDate.getMonth() && currentDate.getYear() == displayedDate.getYear()){
            Date date = new Date();
            Log.d(TAG, "getActualMinimum: " + date.toString());
            return date;
        }
        else{
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(displayedDate);
            calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            calendar.set(Calendar.MILLISECOND, 0);

            Date date = calendar.getTime();
            Log.d(TAG, "getActualMinimum: " + date.toString());
            return date;
        }


    }


    private Date getActualMaximum(){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(displayedDate);
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        Date date = calendar.getTime();
        Log.d(TAG, "getActualMaximum: " + date);
        return date;
    }


    private void addSelectedDates(){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(mSelectedStartDate);

        do{

            Date addedDate = calendar.getTime();

            if(!mDisabledDates.contains(addedDate)){
                mSelectedDates.add(addedDate);
            }
            calendar.add(Calendar.DATE, 1);

        }
        while(calendar.getTime().compareTo(mSelectedEndDate) <= 0);

        for(Date selectedDates: mSelectedDates){
            Log.d(TAG, "addSelectedDates: " + selectedDates.toString());
        }


    }




    private void initializeCalendarNavigation(){
        prevBtn.setOnClickListener(v -> {
            Date newDate = getAvailableDate(AppConstants.SUBTRACT);

            if(calendarPickerView.getVisibility() == View.VISIBLE){
                activity.getAvailableDateFromServer(1900 + newDate.getYear(), newDate.getMonth(), false);
            }

        });

        nextBtn.setOnClickListener(v -> {
            Date newDate = getAvailableDate(AppConstants.ADD);
            if(calendarPickerView.getVisibility() == View.VISIBLE) {
                activity.getAvailableDateFromServer(1900 + newDate.getYear(), newDate.getMonth(), false);
            }
        });
    }

    private void callComputAmountAPI(){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        String checkInStr = format.format(mSelectedStartDate);
        String checkOutStr = format.format(mSelectedEndDate);

        activity.getComputeAmountFromServer(checkInStr, checkOutStr);
    }


    private void setCheckInCheckoutView(){

        String checkInYearStr = (1900 + mSelectedStartDate.getYear()) + "";
        String checkInMonthStr;
        String checkInDayStr;

        int checkInMonthNum = mSelectedStartDate.getMonth() + 1;
        if(checkInMonthNum < 10){
            checkInMonthStr = "0" + checkInMonthNum;
        }
        else{
            checkInMonthStr = checkInMonthNum + "";
        }

        int checkInDayNum = mSelectedStartDate.getDate();
        if(checkInDayNum < 10){
            checkInDayStr = "0" + checkInDayNum;
        }
        else{
            checkInDayStr = checkInDayNum + "";
        }

        checkInDateValueTv.setText(checkInYearStr + "-" +  checkInMonthStr + "-" + checkInDayStr);

        String checkOutYearStr = (mSelectedEndDate.getYear() + 1900) + "";
        String checkOutMonthStr;
        String checkOutDayStr;


        int checkOutMonthNum = mSelectedEndDate.getMonth() + 1;
        if(checkOutMonthNum <10){
            checkOutMonthStr = "0" + checkOutMonthNum;
        }
        else{
            checkOutMonthStr = checkOutMonthNum + "";
        }

        int checkOutDayNum = mSelectedEndDate.getDate();
        if(checkOutDayNum < 10){
            checkOutDayStr = "0" + checkOutDayNum;
        }
        else{
            checkOutDayStr = checkOutDayNum + "";
        }

        checkOutDateValueTv.setText(checkOutYearStr + "-" +  checkOutMonthStr + "-" + checkOutDayStr);

    }
}
