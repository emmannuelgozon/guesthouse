package co.guesthouse.project.main.listing_view;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatRatingBar;
import androidx.recyclerview.widget.RecyclerView;
import co.guesthouse.R;
import co.guesthouse.project.model.SearchResponseListingItem;

public class SearchResultItemAdapter extends RecyclerView.Adapter<SearchResultItemAdapter.SearchResultViewHolder> {

    private static final String TAG = "SearchResultItemAdapter";

    private Context context;
    private List<SearchResponseListingItem> list;
    private GetSpecificCallback callback;

    public SearchResultItemAdapter(Context context, List<SearchResponseListingItem> list,
                                   GetSpecificCallback callback){

        Log.d(TAG, "SearchResultItemAdapter: constructor called");
        this.context = context;
        this.list = list;
        this.callback = callback;

        for(SearchResponseListingItem item: this.list){
            Log.d(TAG, "onCreateView: =====================================");
            Log.d(TAG, "onCreateView: id: " + item.getId());
            Log.d(TAG, "onCreateView: room type: " + item.getRoomType());
            Log.d(TAG, "onCreateView: list name: " + item.getListingName());
            Log.d(TAG, "onCreateView: image: " + item.getImg());
            Log.d(TAG, "onCreateView: bedroom: " + item.getBedroom());
            Log.d(TAG, "onCreateView: guests: " + item.getGuest());
            Log.d(TAG, "onCreateView: price: " + item.getPrice());
            Log.d(TAG, "onCreateView: rate: " + item.getRate());
            Log.d(TAG, "onCreateView: ======================================");
        }

        Log.d(TAG, "SearchResultItemAdapter: constructor list size: " + this.list.size());
    }

    @NonNull
    @Override
    public SearchResultViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Log.d(TAG, "onCreateViewHolder: called");
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.vacation_search_result_item_layout, parent, false);
        return new SearchResultViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SearchResultViewHolder holder, int position) {

        SearchResponseListingItem item = list.get(position);

        Picasso.get().load(item.getImg()).fit().into(holder.imageItemTv);

        holder.roomTypeItemTv.setText(item.getRoomType() +   "\t" + context.getResources().getString(R.string.bullet));
        holder.numberOfBedroomItemTv.setText("\t" + Integer.toString(item.getBedroom())
                + context.getResources().getString(R.string.bedroom_acronym) +"\t" + context.getResources().getString(R.string.bullet));
        holder.numberOfGuestItemTv.setText("\t" + Integer.toString(item.getGuest()) + " "
                + context.getResources().getString(R.string.guest_initial_cap));

        holder.nameItemTv.setText(item.getListingName());
        holder.priceItemTv.setText(context.getResources().getString(R.string.from)
                + " "
                + context.getResources().getString(R.string.dollar_sign)
                + Integer.toString(item.getPrice())
                + " "
                + context.getResources().getString(R.string.per_night));

        float rating = item.getRate();

        holder.ratingBarItemRb.setRating(rating);
        holder.ratingItemTv.setText(Float.toString(rating));

        holder.itemLayout.setOnClickListener(v -> {
            callback.onSpecificClickListener(item);
        });



    }

    @Override
    public int getItemCount() {
        return list.size();
    }




    public static class SearchResultViewHolder extends RecyclerView.ViewHolder{

        RelativeLayout itemLayout;
        AppCompatImageView imageItemTv;
        TextView roomTypeItemTv;
        TextView numberOfBedroomItemTv;
        TextView numberOfGuestItemTv;
        TextView nameItemTv;
        TextView priceItemTv;
        AppCompatRatingBar ratingBarItemRb;
        TextView ratingItemTv;



        public SearchResultViewHolder(@NonNull View itemView) {
            super(itemView);

            Log.d(TAG, "SearchResultViewHolder: constructor called");

            itemLayout = itemView.findViewById(R.id.list_item);
            imageItemTv = itemView.findViewById(R.id.item_image);
            roomTypeItemTv = itemView.findViewById(R.id.item_room_type_tv);
            numberOfBedroomItemTv = itemView.findViewById(R.id.item_number_of_bedroom_tv);
            numberOfGuestItemTv = itemView.findViewById(R.id.item_number_of_guests_tv);
            nameItemTv = itemView.findViewById(R.id.item_name_tv);
            priceItemTv = itemView.findViewById(R.id.item_price_tv);
            ratingBarItemRb = itemView.findViewById(R.id.rating_rb);
            ratingItemTv = itemView.findViewById(R.id.rating_tv);
        }
    }
}
