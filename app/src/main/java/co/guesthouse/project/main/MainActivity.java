package co.guesthouse.project.main;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import butterknife.BindView;
import butterknife.ButterKnife;
import co.guesthouse.R;
import co.guesthouse.project.base.BaseActivity;
import co.guesthouse.project.main.listing_view.VacationResultFragment;
import co.guesthouse.project.main.search_view.VacationHouseSearchFragment;
import co.guesthouse.project.main.specific_view.SpecificListingActivity;
import co.guesthouse.project.model.SearchResponseListingItem;
import co.guesthouse.project.model.SpecificResponse;

public class MainActivity extends BaseActivity implements MainActivityCallback {

    private static final String TAG = "MainActivity";

    @BindView(R.id.navigation)
    BottomNavigationView bottomNavigationView;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getActivityComponent().inject(this);
        ButterKnife.bind(this);
        initialize();
        initializeBottomNavigation();

    }



    @Override
    protected void initialize() {
        setSupportActionBar(toolbar);

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        Fragment childFragment = VacationHouseSearchFragment.newInstance();
        fragmentTransaction.replace(R.id.fragment_main, childFragment);
        fragmentTransaction.commit();
    }

    private void initializeBottomNavigation() {
        bottomNavigationView.setOnNavigationItemSelectedListener(menuItem -> {
            switch(menuItem.getItemId()){
                case R.id.action_book_now:
                    break;
                case R.id.action_my_bookings:
                    Toast.makeText(this, getResources().getString(R.string.not_available_at_the_moment),
                            Toast.LENGTH_SHORT).show();
                    break;
                case R.id.action_contacts:
                    Toast.makeText(this, getResources().getString(R.string.not_available_at_the_moment),
                            Toast.LENGTH_SHORT).show();
                    break;
            }
            return true;
        });
    }


    public void setToolbarTitle(String title){
        TextView tv = toolbar.findViewById(R.id.toolbar_title);
        tv.setText(title);
    }

    @Override
    public void goToResultPage(List<SearchResponseListingItem> listingItemList) {


        Bundle b = new Bundle();
        b.putParcelableArrayList("vacation_searach_result", new ArrayList<>(listingItemList));

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        Fragment childFragment = VacationResultFragment.newInstance();
        childFragment.setArguments(b);
        fragmentTransaction.replace(R.id.fragment_main, childFragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    public void goToSpecificPage(SpecificResponse response, String permalink) {

        Intent intent = new Intent(this, SpecificListingActivity.class);
        intent.putExtra("vacation_specific_item", response);
        intent.putExtra("vacation_specific_permalink", permalink);
        startActivity(intent);

    }
}
