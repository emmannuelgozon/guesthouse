package co.guesthouse.project.main.check_out_view;

import co.guesthouse.project.base.BaseMvpPresenter;
import co.guesthouse.project.base.BaseMvpView;
import co.guesthouse.project.model.CheckOutModel;

public class CheckOutMvp {

    public interface Presenter<V extends CheckOutMvp.View> extends BaseMvpPresenter<V> {
        void checkOut(CheckOutModel checkoutModel);

        void checkoutConfirm(CheckOutModel checkOutModel);
    }


    public interface View extends BaseMvpView{

        void onCheckOutSuccess();

        void onCheckOutFailure();

    }
}
