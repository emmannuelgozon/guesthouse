package co.guesthouse.project.main.search_view;

import android.util.Log;

import javax.inject.Inject;

import co.guesthouse.project.base.BasePresenter;
import co.guesthouse.project.constant.AppConstants;
import co.guesthouse.project.data.ApplicationDataManager;
import co.guesthouse.project.model.SearchVacationResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VacationHouseSearchPresenter<V extends VacationHouseSearchMvp.View>
        extends BasePresenter<V> implements VacationHouseSearchMvp.Presenter<V>
{

    private static final String TAG = "VacationHouseSearchPres";

    @Inject
    public VacationHouseSearchPresenter(ApplicationDataManager dataManager){
        super(dataManager);
        Log.d(TAG, "VacationHouseSearchPresenter: constructor has been called");
    }


    @Override
    public void searchListing(String checkIn, String checkout, String numberOfGuests) {
        Log.d(TAG, "searchListing: checkin: " + checkIn + " checkout: " + checkout + " numberOfGuests: " + numberOfGuests);

        getMvpView().showLoadingDialog();
        Call<SearchVacationResponse> call =  getApplicationDataManager().doPostServerGetListings(checkIn, checkout, numberOfGuests);
        call.enqueue(new Callback<SearchVacationResponse>() {
            @Override
            public void onResponse(Call<SearchVacationResponse> call, Response<SearchVacationResponse> response) {
                Log.d(TAG, "onResponse: response: " + response.toString());
                Log.d(TAG, "onResponse: " +  response.body().toString());

                if(response.isSuccessful() && isViewAttached()){
                    if(response.body().getStatus().equals(AppConstants.SEARCH_RESPONSE_SUCCESS)){
                        getMvpView().onSearchSuccess(response.body().getData());

                        getMvpView().hideLoadingDialog();
                    }
                    else{
                        //TODO: handle not successful from API
                        getMvpView().hideLoadingDialog();
                        getMvpView().showPopUpDialogWithMessage(response.body().getMessage());
                    }
                }
                else{
                    //TODO: handle response http error
                    getMvpView().hideLoadingDialog();
                    getMvpView().showPopUpDialogWithMessage(response.errorBody().toString());
                }


            }

            @Override
            public void onFailure(Call<SearchVacationResponse> call, Throwable t) {
                Log.d(TAG, "onFailure: " + t.getMessage());
                getMvpView().hideLoadingDialog();
                getMvpView().showPopUpDialogWithMessage(t.getMessage());
                getMvpView().onSearchFailure();
            }
        });
    }
}
