package co.guesthouse.project.main.specific_view;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.PorterDuffXfermode;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import co.guesthouse.R;
import co.guesthouse.project.base.BaseActivity;
import co.guesthouse.project.main.booking_view.BookingDialogFragment;
import co.guesthouse.project.main.check_out_view.CheckOutActivity;
import co.guesthouse.project.model.ComputeAmountResponse;
import co.guesthouse.project.model.SpecificResponse;
import co.guesthouse.project.model.SpecificResponseDescItem;

public class SpecificListingActivity extends BaseActivity implements
        SpecificListingMvp.View, ISpecificListingBookingCallback{

    private static final String TAG = "SpecificListingActivity";

    @Inject
    SpecificListingMvp.Presenter<SpecificListingMvp.View> mPresenter;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    /********************* Main layout *******************/
    @BindView(R.id.main_section_layout)
    RelativeLayout mainSectionLayout;
    @BindView(R.id.main_name)
    TextView mainNameTv;
    @BindView(R.id.main_address)
    TextView mainAddressTv;
    @BindView(R.id.main_room_type)
    TextView mainRoomTypeTv;
    @BindView(R.id.main_number_of_guest)
    TextView mainGuestNumberTv;
    @BindView(R.id.main_number_of_bedroom)
    TextView mainBedroomNumberTv;
    @BindView(R.id.main_number_of_beds)
    TextView mainBedsNumberTv;
    @BindView(R.id.main_number_of_bath)
    TextView mainBathNumberTv;
    @BindView(R.id.main_book_now_btn)
    Button mainBookNowBtn;




    /********************* Property Description *******************/


    @BindView(R.id.name_tv)
    TextView nameTv;
    @BindView(R.id.description_tv)
    TextView descriptionTv;


    /********************* Gallery *******************/

    @BindView(R.id.gallery_image_recyclerview)
    RecyclerView galleryRecyclerView;
    RecyclerView.Adapter mGalleryAdapter;

    /********************** MAP **********************/

    SupportMapFragment mapView;


    private BookingDialogFragment bookingDialogFragment;

    private SpecificResponse response;
    private String mPermalink;
    private String checkIn;
    private String checkOut;

    private ComputeAmountResponse mComputAmountResponse;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.specific_listing_layout);
        getActivityComponent().inject(this);
        ButterKnife.bind(this);
        mPresenter.onAttach(this);

        Intent intent = getIntent();
        if(intent.hasExtra("vacation_specific_item")){

            response = intent.getParcelableExtra("vacation_specific_item");
            mPermalink = intent.getStringExtra("vacation_specific_permalink");
            Log.d(TAG, "onCreate: " + response.toString());

            TextView titleTv = toolbar.findViewById(R.id.toolbar_title);
            titleTv.setText(response.getName());

            initialize();
        }
    }




    @Override
    protected void initialize() {
        initializeMainLayout();
    }


    private void initializeMainLayout(){

        // setting layout:background
        ImageView img = new ImageView(this);

        Log.d(TAG, "initializeMainLayout: main image: " + response.getMainImage());


        final int GRADIENT_HEIGHT = 32;
        final Target target = new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                Log.d(TAG, "onBitmapLoaded: ");

                int w = bitmap.getWidth();
                int h = bitmap.getHeight();
                Bitmap overlay = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
                Canvas canvas = new Canvas(overlay);

                canvas.drawBitmap(bitmap, 0, 0, null);

                Paint paint = new Paint();

                ColorMatrix colorMatrix = new ColorMatrix();
                colorMatrix.setSaturation(1f);
                ColorMatrix colorScale = new ColorMatrix();
                colorScale.setScale(1, 0.85f, 1, 1);
                colorMatrix.postConcat(colorScale);
                paint.setColorFilter(new ColorMatrixColorFilter(colorMatrix));
                canvas.drawBitmap(bitmap, 0, 0, paint);

                mainSectionLayout.setBackground(new BitmapDrawable(getResources(), overlay));
            }

            @Override
            public void onBitmapFailed(Exception e, Drawable errorDrawable) {
                Log.d(TAG, "onBitmapFailed: " + e.getMessage());

                mainNameTv.setTextColor(getResources().getColor(R.color.grey));
                mainAddressTv.setTextColor(getResources().getColor(R.color.grey));
                mainRoomTypeTv.setTextColor(getResources().getColor(R.color.grey));
                mainGuestNumberTv.setTextColor(getResources().getColor(R.color.grey));
                mainBedroomNumberTv.setTextColor(getResources().getColor(R.color.grey));
                mainBedsNumberTv.setTextColor(getResources().getColor(R.color.grey));
                mainBathNumberTv.setTextColor(getResources().getColor(R.color.grey));
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {
                Log.d(TAG, "onPrepareLoad: onPrepareLoad");
            }
        };


        Picasso.get().load(response.getMainImage())
                .resize(300, 300)
                .centerCrop()
                .into(target);

        mainSectionLayout.setTag(target);

        mainNameTv.setText(response.getName());
        mainAddressTv.setText(response.getAddress());

        mainRoomTypeTv.setText(response.getRoomtype() + " ");
        mainGuestNumberTv.setText(" " + Integer.toString(response.getGuest()) + " "
                + getResources().getString(R.string.guests) + " ");
        mainBedroomNumberTv.setText(Integer.toString(response.getBedroom()) + " "
                + getResources().getString(R.string.bedroom) + " ");
        mainBedsNumberTv.setText(Integer.toString(response.getBed()) + " "
                +  getResources().getString(R.string.beds) + " ");
        mainBathNumberTv.setText(Integer.toString(response.getBathroom()) + " "
                + getResources().getString(R.string.bath));

        SpecificResponseDescItem descItem = new SpecificResponseDescItem();

        List<SpecificResponseDescItem> descItemList = response.getDesc();

        Log.d(TAG, "initializeMainLayout: size: " + descItemList.size());
        for(SpecificResponseDescItem itemIndex: descItemList){
            descItem = itemIndex;
            break;
        }


        // property description
        nameTv.setText(descItem.getTitle());
        descriptionTv.setText(descItem.getDesc());


        // gallery
        mGalleryAdapter = new GalleryAdapter(this, response.getImg());
        galleryRecyclerView.setAdapter(mGalleryAdapter);
        galleryRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        galleryRecyclerView.setHasFixedSize(true);


        //map
        mapView = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapView);
        mapView.getMapAsync(googleMap -> {
            Double longitude = Double.parseDouble(response.getJsonMemberLong());
            Double latitide = Double.parseDouble(response.getLat());
            LatLng coordinates = new LatLng(latitide, longitude);

            googleMap.addMarker(new MarkerOptions().position(coordinates).title(response.getName()));
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(coordinates, 16.0f));
        });


        mainBookNowBtn.setOnClickListener( v -> {

            Date date = new Date();
            int currentYear = 1900 + date.getYear();

            Log.d(TAG, "initializeMainLayout: current year: " +  currentYear + " current month: " +date.getMonth());

            FragmentManager manager = getSupportFragmentManager();
            bookingDialogFragment = BookingDialogFragment.newInstance();
            bookingDialogFragment.getArguments();
            bookingDialogFragment.show(manager, "booking_dialog_fragment");

        });

    }



    @Override
    public void getAvailableDateFromServer(int year, int month, boolean isInitCalendar) {
        mPresenter.getAvailableDates(mPermalink, year, month + 1, isInitCalendar);
    }

    @Override
    public void getComputeAmountFromServer(String checkIn, String checkOut) {
        this.checkIn = checkIn;
        this.checkOut = checkOut;
        mPresenter.getComputeAmount(mPermalink, checkIn, checkOut);
    }


    @Override
    public void onAvailableDateResponseSuccess(List<Date> disableDays, int year, int month) {


        if(bookingDialogFragment != null && bookingDialogFragment.getShowsDialog()){
            bookingDialogFragment.setCalendar(disableDays, year, month);
        }
        else{
            FragmentManager manager = getSupportFragmentManager();
            bookingDialogFragment = BookingDialogFragment.newInstance();
            bookingDialogFragment.getArguments();
            bookingDialogFragment.show(manager, "booking_dialog_fragment");
        }
    }

    @Override
    public void onAvailableDateResponseFailure() {

    }

    @Override
    public void onComputeAmountResponseSuccess(ComputeAmountResponse response) {
        mComputAmountResponse = response;
        bookingDialogFragment.setBookingDetails(response);
    }

    @Override
    public void onComputeAmountResponseFailure() {

    }


    @Override
    public void proceedToCheckout() {

        Bundle b = new Bundle();
        b.putParcelable("price_summary", mComputAmountResponse);
        b.putString("permalink", mPermalink);
        b.putString("check_in", checkIn);
        b.putString("check_out", checkOut);

        Intent intent = new Intent(this, CheckOutActivity.class);
        intent.putExtras(b);

        startActivity(intent);
    }

    @Override
    public void checkReservationDates() {
        mPresenter.checkReservationDates(mPermalink, checkIn, checkOut);
    }

}
