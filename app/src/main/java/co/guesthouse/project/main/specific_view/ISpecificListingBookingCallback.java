package co.guesthouse.project.main.specific_view;

public interface ISpecificListingBookingCallback {


    void getAvailableDateFromServer(int year, int month, boolean isInitCalendar);

    void getComputeAmountFromServer(String checkIn, String checkOut);

    void checkReservationDates();


}
