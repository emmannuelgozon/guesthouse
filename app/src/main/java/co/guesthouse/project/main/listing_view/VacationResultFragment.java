package co.guesthouse.project.main.listing_view;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import co.guesthouse.R;
import co.guesthouse.project.base.BaseFragment;
import co.guesthouse.project.dependency_injection.ActivityComponent;
import co.guesthouse.project.main.MainActivity;
import co.guesthouse.project.model.SearchResponseListingItem;
import co.guesthouse.project.model.SpecificResponse;

public class VacationResultFragment extends BaseFragment implements
        VacationResultMvp.View,
        GetSpecificCallback{

    private static final String TAG = "VacationResultFragment";

    @Inject
    VacationResultMvp.Presenter<VacationResultMvp.View> mPresenter;

    @BindView(R.id.search_result_recycler_view)
    RecyclerView mRecyclerView;

    private RecyclerView.Adapter mAdapter;
    private List<SearchResponseListingItem> listItem;


    private MainActivity mActivity;
    private Context mContext;

    public static VacationResultFragment newInstance(){
        Bundle bundle = new Bundle();
        VacationResultFragment fragment = new VacationResultFragment();
        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if(context != null){

            mContext = context;

            if(context instanceof MainActivity){
                mActivity = (MainActivity) context;
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.vacation_search_result_layout, null,false);
        ActivityComponent component = getActivityComponent();
        if(component != null){
            ButterKnife.bind(this, view);
            getActivityComponent().inject(this);
            mPresenter.onAttach(this);

        }

        mActivity.setToolbarTitle(getResources().getString(R.string.all_properties));

        listItem = getArguments().getParcelableArrayList("vacation_searach_result");

        initialize();
        return view;
    }

    @Override
    protected void initialize() {

        mAdapter = new SearchResultItemAdapter(mContext, listItem,this);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setHasFixedSize(true);
    }

    @Override
    public void onGetSpecificListingSuccess(SpecificResponse response, String permalink) {
        //todo: go to specific page
        mActivity.goToSpecificPage(response, permalink);
    }

    @Override
    public void onGetSpecificListingFailure() {
        //todo: what to do?
    }

    @Override
    public void onSpecificClickListener(SearchResponseListingItem item) {
        mPresenter.getSpecificListingData(item.getPermalink());
    }
}
