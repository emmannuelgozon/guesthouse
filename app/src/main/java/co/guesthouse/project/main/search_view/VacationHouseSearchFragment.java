package co.guesthouse.project.main.search_view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;
import butterknife.BindView;
import butterknife.ButterKnife;
import co.guesthouse.R;
import co.guesthouse.project.base.BaseFragment;
import co.guesthouse.project.constant.AppConstants;
import co.guesthouse.project.dependency_injection.ActivityComponent;
import co.guesthouse.project.model.SearchResponseListingItem;
import co.guesthouse.project.main.MainActivity;
import co.guesthouse.project.util.DatePickerFragment;

import static android.app.Activity.RESULT_OK;

public class VacationHouseSearchFragment extends BaseFragment
        implements VacationHouseSearchMvp.View{

    private static final String TAG = "VacationHouseSearchFrag";

    @BindView(R.id.datepicker_checkin_tv)
    TextView checkInTv;

    @BindView(R.id.datepicker_checkout_tv)
    TextView checkOutTv;

    @BindView(R.id.location_spinner)
    Spinner locationSpinner;

    @BindView(R.id.guest_input_tv)
    TextView guestInputTv;

    @BindView(R.id.search_btn)
    Button searchBtn;

    private Context mContext;
    private MainActivity mActivity;


    private String checkInDateStr;
    private String checkOutDateStr;
    private int mNumberOfGuests = 1;

    @Inject
    VacationHouseSearchMvp.Presenter<VacationHouseSearchMvp.View> mPresenter;


    public static VacationHouseSearchFragment newInstance(){
        Bundle bundle = new Bundle();
        VacationHouseSearchFragment fragment = new VacationHouseSearchFragment();
        fragment.setArguments(bundle);

        return fragment;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if(context != null){

            mContext = context;

            if(context instanceof MainActivity){
                mActivity = (MainActivity) context;
            }
        }
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();

        checkInDateStr = null;
        checkOutDateStr = null;
        mNumberOfGuests = 1;
    }

    @Override
    public View onCreateView( LayoutInflater inflater,ViewGroup container,Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.vacation_house_search_fragment, null,false);

        ActivityComponent component = getActivityComponent();
        if(component != null){
            ButterKnife.bind(this, view);
            getActivityComponent().inject(this);
            mPresenter.onAttach(this);
        }

        mActivity.setToolbarTitle(getResources().getString(R.string.app_name));

        initialize();
        return view;
    }

    @Override
    protected void initialize() {

        popupateSpinner();
        populateNumberOfGuests();


        checkInTv.setOnClickListener(v -> {
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            DialogFragment checkInDialog = new DatePickerFragment();
            checkInDialog.setTargetFragment(this, AppConstants.DATEPICKER_CHECKIN_REQUEST_CODE);
            checkInDialog.show(fragmentManager, "checkInDatePicker");
        });

        checkOutTv.setOnClickListener(v -> {
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            DialogFragment checkOutDialog = new DatePickerFragment();
            checkOutDialog.setTargetFragment(this, AppConstants.DATEPICKER_CHECKOUT_REQUEST_CODE);
            checkOutDialog.show(fragmentManager, "checkInDatePicker");
        });

        searchBtn.setOnClickListener(v -> {
            Log.d(TAG, "initialize: checkIn: " + checkInDateStr + " checkout: " + checkOutDateStr + " guests: " + mNumberOfGuests);
            mPresenter.searchListing(checkInDateStr, checkOutDateStr, mNumberOfGuests + "");
        });
    }


    private void popupateSpinner(){
        List<String> locationList = new ArrayList<>();
        locationList.add("Singapore");

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_dropdown_item

                , locationList);
        locationSpinner.setAdapter(adapter);
    }


    private void populateNumberOfGuests(){

        guestInputTv.setText(Integer.toString(mNumberOfGuests));

        guestInputTv.setOnClickListener(
                v -> {
                    LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                    final View popUpView = inflater.inflate(R.layout.number_of_guests_layout, null);

                    PopupWindow popupWindow = new PopupWindow(popUpView, guestInputTv.getWidth(), ViewGroup.LayoutParams.WRAP_CONTENT);
                    popupWindow.setOutsideTouchable(true);

                    Button addGuestBtn = popUpView.findViewById(R.id.add_guest_number_btn);
                    Button removeGuestBtn = popUpView.findViewById(R.id.remove_guest_number_btn);
                    TextView numberOfGuestsTv = popUpView.findViewById(R.id.number_of_guests_tv);
                    numberOfGuestsTv.setText(Integer.toString(mNumberOfGuests));

                    addGuestBtn.setOnClickListener(w -> {
                        mNumberOfGuests++;
                        numberOfGuestsTv.setText(Integer.toString(mNumberOfGuests));
                        guestInputTv.setText(Integer.toString(mNumberOfGuests));
                    });

                    removeGuestBtn.setOnClickListener(w -> {
                        if(mNumberOfGuests > 0) mNumberOfGuests--;
                        numberOfGuestsTv.setText(Integer.toString(mNumberOfGuests));
                        guestInputTv.setText(Integer.toString(mNumberOfGuests));
                    });

                    popupWindow.setOnDismissListener(() -> {
                        guestInputTv.setText(Integer.toString(mNumberOfGuests));
                    });

                    popupWindow.showAsDropDown(guestInputTv);

                }
        );
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == RESULT_OK){

            Bundle bundle = data.getExtras();
            String year = bundle.getString("year");
            String month = bundle.getString("month");
            String day = bundle.getString("day");

            String setToText= month + "-" + day + "-" + year;

            switch(requestCode){
                case AppConstants.DATEPICKER_CHECKIN_REQUEST_CODE:
                    checkInTv.setText(setToText);
                    checkInDateStr = year + "-" + month + "-" +day;
                    break;
                case AppConstants.DATEPICKER_CHECKOUT_REQUEST_CODE:
                    checkOutTv.setText(setToText);
                    checkOutDateStr = year + "-" + month + "-" +day;
                    break;

            }
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mContext = null;
        mActivity = null;
    }


    @Override
    public void onSearchSuccess(List<SearchResponseListingItem> listingItemList) {
        mActivity.goToResultPage(listingItemList);
    }

    @Override
    public void onSearchFailure() {

    }
}
