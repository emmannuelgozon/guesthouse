package co.guesthouse.project.main.listing_view;

import co.guesthouse.project.model.SearchResponseListingItem;

public interface GetSpecificCallback {

    void onSpecificClickListener(SearchResponseListingItem item);
}
