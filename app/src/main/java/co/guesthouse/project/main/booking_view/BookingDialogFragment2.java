package co.guesthouse.project.main.booking_view;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.roomorama.caldroid.CaldroidFragment;
import com.roomorama.caldroid.CaldroidListener;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentTransaction;
import butterknife.ButterKnife;
import co.guesthouse.R;
import co.guesthouse.project.base.BaseDialogFragment;
import co.guesthouse.project.dependency_injection.ActivityComponent;

public class BookingDialogFragment2 extends BaseDialogFragment{

    private Context context;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        return inflater.inflate(R.layout.booking_dialog_fragment_layout_2, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        ActivityComponent component = getActivityComponent();
        if(component != null){
            ButterKnife.bind(this, view);
            getActivityComponent().inject(this);
        }


        initialize();

    }

    @Override
    protected void initialize() {
        initializeCalendar();
    }


    private void initializeCalendar(){
        CaldroidFragment caldroidFragment = new CaldroidFragment();
        Bundle args = new Bundle();
        Calendar cal = Calendar.getInstance();
        args.putInt(CaldroidFragment.MONTH, cal.get(Calendar.MONTH) + 1);
        args.putInt(CaldroidFragment.YEAR, cal.get(Calendar.YEAR));
        caldroidFragment.setArguments(args);

        ArrayList<Date> disableDateList = new ArrayList<>();
        for(int i = 0; i < 7; i++){
            cal.set(Calendar.YEAR, 2019);
            cal.set(Calendar.MONTH, Calendar.FEBRUARY);
            cal.set(Calendar.DAY_OF_MONTH, 22 + i);
            cal.set(Calendar.HOUR_OF_DAY, 0);
            cal.set(Calendar.MINUTE, 0);
            cal.set(Calendar.SECOND, 0);
            cal.set(Calendar.MILLISECOND, 0);
            Date d = cal.getTime();
            disableDateList.add(d);
        }

        cal.set(Calendar.YEAR, 2019);
        cal.set(Calendar.MONTH, Calendar.MARCH);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        Date d = cal.getTime();
        disableDateList.add(d);

        caldroidFragment.setDisableDates(disableDateList);


        final CaldroidListener caldroidListener = new CaldroidListener() {
            int pressCounter = -1;
            Date checkInDate;
            Date checkOutDate;
            View checkInView;
            View checkOutView;

            DateFormat format = DateFormat.getDateInstance();

            @Override
            public void onSelectDate(Date date, View view) {
                pressCounter++;


                // from
                if(pressCounter % 2 == 0){
                    if(checkInView != null)
                        checkInView.setBackgroundColor(context.getResources().getColor(R.color.caldroid_white));

                    if(checkOutView != null)
                        checkOutView.setBackgroundColor(context.getResources().getColor(R.color.caldroid_white));


                    checkInView = view;
                    checkInDate = date;

                    //resetting values
                    checkInView.setBackgroundColor(context.getResources().getColor(R.color.caldroid_holo_blue_light));

                }
                // to
                else{
                    checkOutView = view;
                    checkOutDate = date;

                    //checking if checkoutdate is greater than checkin date
                    if(checkOutDate.after(checkInDate)){

                        checkOutView.setBackgroundColor(context.getResources().getColor(R.color.caldroid_light_red));
//                        long checkInLong = checkInDate.getTime();
//                        long checkoutLong = checkOutDate.getTime();
//                        long difference = checkoutLong - checkInLong;


                    }

                }
            }


            @Override
            public void onChangeMonth(int month, int year) {
                super.onChangeMonth(month, year);


            }
        };
        caldroidFragment.setCaldroidListener(caldroidListener);

        //setting minDate on current day
        Date today = new Date();
        caldroidFragment.setMinDate(today);

        FragmentTransaction ft = getChildFragmentManager().beginTransaction();
        ft.replace(R.id.calendar_container, caldroidFragment);
        ft.commit();
    }
}
