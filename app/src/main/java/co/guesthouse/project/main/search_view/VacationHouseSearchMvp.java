package co.guesthouse.project.main.search_view;

import java.util.List;

import co.guesthouse.project.base.BaseMvpPresenter;
import co.guesthouse.project.base.BaseMvpView;
import co.guesthouse.project.model.SearchResponseListingItem;

public class VacationHouseSearchMvp {



    public interface Presenter<V extends VacationHouseSearchMvp.View> extends BaseMvpPresenter<V> {

        void searchListing(String checkIn, String checkout, String numberOfGuests);

    }

    public interface View extends BaseMvpView {

        void onSearchSuccess(List<SearchResponseListingItem> listingItemList);

        void onSearchFailure();

    }




}
