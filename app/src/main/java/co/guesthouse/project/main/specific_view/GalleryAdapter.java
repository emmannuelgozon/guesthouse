package co.guesthouse.project.main.specific_view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;
import co.guesthouse.R;
import co.guesthouse.project.main.listing_view.SearchResultItemAdapter;

public class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.GalleryImageHolder> {

    Context context;
    List<String> imageUrl;

    public GalleryAdapter(Context context, List<String> imageUrl){
        this.context = context;
        this.imageUrl = imageUrl;
    }


    @NonNull
    @Override
    public GalleryAdapter.GalleryImageHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.gallery_item, parent, false);
        return new GalleryAdapter.GalleryImageHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull GalleryImageHolder holder, int position) {
        String imageUrlItem = imageUrl.get(position);

        Picasso.get().load(imageUrlItem).fit().into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return imageUrl.size();
    }


    public static class GalleryImageHolder extends RecyclerView.ViewHolder {

        AppCompatImageView imageView;

        public GalleryImageHolder(@NonNull View itemView) {
            super(itemView);

            imageView = itemView.findViewById(R.id.gallery_image_item);
        }
    }


}
