package co.guesthouse.project.main.check_out_view;

import android.util.Log;

import javax.inject.Inject;

import co.guesthouse.project.base.BasePresenter;
import co.guesthouse.project.data.ApplicationDataManager;
import co.guesthouse.project.model.CheckOutModel;
import co.guesthouse.project.model.CheckOutResponse;
import co.guesthouse.project.model.checkout_confirm.CheckOutConfirmResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CheckOutPresenter<V extends CheckOutMvp.View>
        extends BasePresenter<V> implements CheckOutMvp.Presenter<V> {

    private static final String TAG = "CheckOutPresenter";

    @Inject
    public CheckOutPresenter(ApplicationDataManager applicationDataManager) {
        super(applicationDataManager);
    }

    @Override
    public void checkOut(CheckOutModel checkoutModel) {

        getMvpView().showLoadingDialog();

        Call<CheckOutResponse> call = getApplicationDataManager().doPostServerCheckout(
                checkoutModel
        );

        call.enqueue(new Callback<CheckOutResponse>() {
            @Override
            public void onResponse(Call<CheckOutResponse> call, Response<CheckOutResponse> response) {

                if(response.isSuccessful()){

                    CheckOutResponse checkOutResponse = response.body();

                    String status = checkOutResponse.getStatus();
                    if(status.equals("success")){
                        Log.d(TAG, "onResponse: success: " + checkOutResponse.getToken());

                        //should not dismiss loading dialog and continue to next process
                        checkoutConfirm(checkoutModel);
                    }
                    else if(status.equals("error")){
                        Log.d(TAG, "onResponse: error: " + checkOutResponse.getMessage());
                        getMvpView().hideLoadingDialog();
                        getMvpView().showPopUpDialogWithMessage(checkOutResponse.getMessage());

                    }
                }
                else{
                    getMvpView().hideLoadingDialog();
                    getMvpView().showPopUpDialogWithMessage(response.errorBody().toString());
                }
            }

            @Override
            public void onFailure(Call<CheckOutResponse> call, Throwable t) {
                getMvpView().hideLoadingDialog();
                getMvpView().showPopUpDialogWithMessage(t.getMessage());
            }
        });

    }

    @Override
    public void checkoutConfirm(CheckOutModel checkoutModel) {

        Call<CheckOutConfirmResponse> call = getApplicationDataManager().doPostServerCheckoutConfirm(
                checkoutModel
        );


        call.enqueue(new Callback<CheckOutConfirmResponse>() {
            @Override
            public void onResponse(Call<CheckOutConfirmResponse> call, Response<CheckOutConfirmResponse> response) {
                if(response.isSuccessful()){

                    CheckOutConfirmResponse checkOutResponse = response.body();

                    String status = checkOutResponse.getStatus();
                    if(status.equals("success")){
                        Log.d(TAG, "onResponse: success: " + checkOutResponse.getMessage());
                        getMvpView().hideLoadingDialog();
                        getMvpView().showPopUpDialogWithMessage(checkOutResponse.getMessage());
                    }
                    else if(status.equals("error")){
                        Log.d(TAG, "onResponse: error: " + checkOutResponse.getMessage());
                        getMvpView().hideLoadingDialog();
                        getMvpView().showPopUpDialogWithMessage(checkOutResponse.getMessage());
                    }

                }
                else{
                    getMvpView().hideLoadingDialog();
                    getMvpView().showPopUpDialogWithMessage(response.errorBody().toString());
                }
            }

            @Override
            public void onFailure(Call<CheckOutConfirmResponse> call, Throwable t) {
                getMvpView().hideLoadingDialog();
                getMvpView().showPopUpDialogWithMessage(t.getMessage());
            }
        });


    }
}
