package co.guesthouse.project.main.listing_view;

import co.guesthouse.project.base.BaseMvpPresenter;
import co.guesthouse.project.base.BaseMvpView;
import co.guesthouse.project.model.SpecificResponse;

public class VacationResultMvp {


    public interface Presenter<V extends VacationResultMvp.View> extends BaseMvpPresenter<V> {

        void getSpecificListingData(String permalink);
    }


    public interface View extends BaseMvpView {
        void onGetSpecificListingSuccess(SpecificResponse response, String permalink);

        void onGetSpecificListingFailure();
    }

}
