package co.guesthouse.project.main;

import java.util.List;

import co.guesthouse.project.model.SearchResponseListingItem;
import co.guesthouse.project.model.SpecificResponse;

public interface MainActivityCallback {
    void goToResultPage(List<SearchResponseListingItem> listingItemList);

    void goToSpecificPage(SpecificResponse response, String permalink);
}
