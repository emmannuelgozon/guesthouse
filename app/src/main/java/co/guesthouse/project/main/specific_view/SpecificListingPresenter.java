package co.guesthouse.project.main.specific_view;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import co.guesthouse.project.base.BasePresenter;
import co.guesthouse.project.data.ApplicationDataManager;
import co.guesthouse.project.model.ComputeAmountItems;
import co.guesthouse.project.model.ComputeAmountProcessFee;
import co.guesthouse.project.model.ComputeAmountResponse;
import co.guesthouse.project.model.ComputeAmountTotal;
import co.guesthouse.project.model.check_reservation_dates.ReservationDatesResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SpecificListingPresenter<V extends SpecificListingMvp.View>
        extends BasePresenter<V> implements SpecificListingMvp.Presenter<V>{

    private static final String TAG = SpecificListingPresenter.class.getName();

    @Inject
    public SpecificListingPresenter(ApplicationDataManager applicationDataManager) {
        super(applicationDataManager);
        Log.d(TAG, "SpecificListingPresenter: constructor is called");
    }




    @Override
    public void getAvailableDates(String permalink, int year, int month, boolean isInitCalendar) {

        Log.d(TAG, "getAvailableDates: permalink: " + permalink  + " year: " + year + " month: "
                + month + " initcalendar: " + isInitCalendar);

        if(isInitCalendar) getMvpView().showLoadingDialog();

        String yearStr = Integer.toString(year);
        String monthStr = Integer.toString(month);

        Call<JsonArray> call = getApplicationDataManager().doPostServerGetSpecificAvailableDates(
                permalink, yearStr, monthStr
        );

        call.enqueue(new Callback<JsonArray>() {

            @Override
            public void onResponse(Call<JsonArray> call, Response<JsonArray> response) {
                if(response.isSuccessful()){
                    Log.d(TAG, "onResponse:  " + response.body());

                    List<Date> disableDays = new ArrayList<>();
                    DateFormat format = new SimpleDateFormat("yyyy-MM-dd");

                    JsonArray array = response.body();

                    if(array.size() > 0){

                        try {
                            for (JsonElement element : array) {
                                String disabledDateStr = element.getAsString();
                                Date date = format.parse(disabledDateStr);
                                disableDays.add(date);
                            }
                        }
                        catch (ParseException e) {
                            e.printStackTrace();
                            if(isInitCalendar) getMvpView().hideLoadingDialog();
                            getMvpView().showPopUpDialogWithMessage(e.getMessage());
                        }
                    }

                    getMvpView().onAvailableDateResponseSuccess(disableDays, year, month);
                }
                else{
                    if(isInitCalendar) getMvpView().hideLoadingDialog();
                    getMvpView().showPopUpDialogWithMessage(response.errorBody().toString());
                }

                if(isInitCalendar) getMvpView().hideLoadingDialog();
            }

            @Override
            public void onFailure(Call<JsonArray> call, Throwable t) {
                Log.d(TAG, "onFailure: " + t.getMessage());
                if(isInitCalendar) getMvpView().hideLoadingDialog();
                getMvpView().showPopUpDialogWithMessage(t.getMessage());

            }
        });
    }





    @Override
    public void getComputeAmount(String permalink, String checkIn, String checkOut) {

        Log.d(TAG, "getComputeAmount: permalink: " + permalink + " checkIn: " + checkIn
                + " checkOut: " + checkOut);

        Call<JsonArray> call = getApplicationDataManager().doPostServerGetComputeAmount(
                permalink, checkIn, checkOut
        );

        call.enqueue(new Callback<JsonArray>() {
            @Override
            public void onResponse(Call<JsonArray> call, Response<JsonArray> response) {

                ComputeAmountResponse computeAmountResponse = new ComputeAmountResponse();


                Log.d(TAG, "onResponse: " + response.body());

                if(response.isSuccessful()){
                    JsonArray array = response.body();

                    if(array.size() > 0){

                        GsonBuilder gsonBuilder = new GsonBuilder();
                        gsonBuilder.setPrettyPrinting();
                        Gson gson = gsonBuilder.create();

                        for (JsonElement responseElement : array) {
                            
                            JsonObject object = responseElement.getAsJsonObject();

                            int counter = 0;
                            Log.d(TAG, "onResponse: counter: " + ++counter);

                            if(object.has("items")){

                                JsonArray itemArray = object.getAsJsonArray("items");
                                List<ComputeAmountItems> computeAmountItemsList = new ArrayList<>();

                                for(JsonElement objectItem : itemArray){

                                    String objectItemStr = objectItem.toString();
                                    ComputeAmountItems computeAmountItems = gson.fromJson(objectItemStr, ComputeAmountItems.class);
                                    computeAmountItemsList.add(computeAmountItems);
                                }

                                computeAmountResponse.setItems(computeAmountItemsList);
                            }

                            if(object.has("process_fee") && object.has("subtotal")){

                                String processFeeStr = object.toString();

                                ComputeAmountProcessFee process = gson.fromJson(processFeeStr, ComputeAmountProcessFee.class);
                                computeAmountResponse.setProcessFee(process);
                            }

                            if(object.has("total")){

                                String totalStr = object.toString();

                                ComputeAmountTotal total = gson.fromJson(totalStr, ComputeAmountTotal.class);
                                computeAmountResponse.setTotal(total);
                            }
                        }


                        getMvpView().onComputeAmountResponseSuccess(computeAmountResponse);

                        Log.d(TAG, "onResponse: computeamountresponse: " + computeAmountResponse.toString());
                        
                    }
                }
                else{
                    getMvpView().showPopUpDialogWithMessage(response.errorBody().toString());
                }

            }

            @Override
            public void onFailure(Call<JsonArray> call, Throwable t) {
                Log.d(TAG, "onFailure: " + t.getMessage());
            }
        });

    }

    @Override
    public void checkReservationDates(String permalink, String checkIn, String checkOut) {

        getMvpView().showLoadingDialog();

        Log.d(TAG, "checkReservationDates: permalink: " + permalink + " checkIn: " + checkIn
                + " checkOut: " + checkOut);

        Call<ReservationDatesResponse> call = getApplicationDataManager().doPostServerCheckReservationDates(
                permalink, checkIn, checkOut
        );

        call.enqueue(new Callback<ReservationDatesResponse>() {
            @Override
            public void onResponse(Call<ReservationDatesResponse> call, Response<ReservationDatesResponse> response) {

                if(response.isSuccessful()){
                    String message = response.body().getMessage();
                    String status = response.body().getStatus();

                    Log.d(TAG, "onResponse: message: " + message);
                    if(status.equals("success")){
                        getMvpView().proceedToCheckout();
                    }
                    else if(status.equals("error")){
                        getMvpView().showPopUpDialogWithMessage(message);
                    }
                    getMvpView().hideLoadingDialog();
                }
                else{
                    getMvpView().hideLoadingDialog();
                    getMvpView().showPopUpDialogWithMessage(response.errorBody().toString());
                }
            }

            @Override
            public void onFailure(Call<ReservationDatesResponse> call, Throwable t) {
                getMvpView().hideLoadingDialog();
                getMvpView().showPopUpDialogWithMessage(t.toString());
            }
        });
    }

}
