package co.guesthouse.project.main.specific_view;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import co.guesthouse.project.base.BaseMvpPresenter;
import co.guesthouse.project.base.BaseMvpView;
import co.guesthouse.project.model.ComputeAmountResponse;

public class SpecificListingMvp {

    public interface View extends BaseMvpView {

        void onAvailableDateResponseSuccess(List<Date> disableDays, int year, int month);

        void onAvailableDateResponseFailure();

        void onComputeAmountResponseSuccess(ComputeAmountResponse response);

        void onComputeAmountResponseFailure();

        void proceedToCheckout();
    }


    public interface Presenter<V extends SpecificListingMvp.View> extends BaseMvpPresenter<V>{

        void getAvailableDates(String permalink, int year, int month, boolean isInitCalendar);

        void getComputeAmount(String permalink, String checkIn, String checkOut);

        void checkReservationDates(String permalink, String checkIn, String checkOut);

    }
}
