package co.guesthouse.project.main.listing_view;

import android.util.Log;

import javax.inject.Inject;

import co.guesthouse.project.base.BasePresenter;
import co.guesthouse.project.data.ApplicationDataManager;
import co.guesthouse.project.model.SpecificResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VacationResultPresenter<V extends VacationResultMvp.View>
        extends BasePresenter<V> implements VacationResultMvp.Presenter<V> {

    private static final String TAG = "VacationResultPresenter";

    @Inject
    public VacationResultPresenter(ApplicationDataManager applicationDataManager){
        super(applicationDataManager);
    }


    @Override
    public void getSpecificListingData(String permalink) {

        Call<SpecificResponse> call = getApplicationDataManager().doPostServerGetSpecific(permalink);

        getMvpView().showLoadingDialog();
        call.enqueue(new Callback<SpecificResponse>() {
            @Override
            public void onResponse(Call<SpecificResponse> call, Response<SpecificResponse> response) {
                Log.d(TAG, "onResponse: " + response.toString());
                Log.d(TAG, "onResponse: body: " + response.body().toString());

                if(response.isSuccessful()){
                        getMvpView().onGetSpecificListingSuccess(response.body(), permalink);
                }
                else{
                    //TODO: handle response http error
                    Log.d(TAG, "onResponse: http error");
                    Log.d(TAG, "onResponse: ");
                    getMvpView().showPopUpDialogWithMessage(response.errorBody().toString());
                }

                getMvpView().hideLoadingDialog();

            }

            @Override
            public void onFailure(Call<SpecificResponse> call, Throwable t) {
                Log.d(TAG, "onFailure: " + t.getMessage());
                getMvpView().hideLoadingDialog();
                getMvpView().showPopUpDialogWithMessage(t.toString());
            }
        });

    }
}
