package co.guesthouse.project.dependency_injection;

import android.app.Application;
import android.content.Context;

import javax.inject.Singleton;

import co.guesthouse.project.data.ApplicationDataManager;
import co.guesthouse.project.data.network.ApiHelper;
import dagger.Module;
import dagger.Provides;

@Module
public class ApplicationModule {

    private final Application mApplication;

    public ApplicationModule(Application application){
        mApplication = application;
    }

    @Provides
    Context provideContext(){
        return mApplication;
    }

    @Provides
    Application provideApplication(){
        return mApplication;
    }

    @Provides
    @Singleton
    ApplicationDataManager provideApplicationDataManager(ApplicationDataManager applicationDataManager){
        return applicationDataManager;
    }

    @Provides
    @Singleton
    ApiHelper provideApiHelper(ApiHelper apiHelper){
        return apiHelper;
    }
}
