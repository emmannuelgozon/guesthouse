package co.guesthouse.project.dependency_injection;


import co.guesthouse.project.base.BaseActivity;
import co.guesthouse.project.main.MainActivity;
import co.guesthouse.project.main.booking_view.BookingDialogFragment;
import co.guesthouse.project.main.booking_view.BookingDialogFragment2;
import co.guesthouse.project.main.check_out_view.CheckOutActivity;
import co.guesthouse.project.main.listing_view.VacationResultFragment;
import co.guesthouse.project.main.search_view.VacationHouseSearchFragment;
import co.guesthouse.project.main.specific_view.SpecificListingActivity;
import dagger.Component;

@ActivityScope
@Component(modules = ActivityModule.class, dependencies = ApplicationComponent.class)
public interface ActivityComponent {

    void inject(BaseActivity activity);

    void inject(MainActivity activity);

    void inject(VacationHouseSearchFragment fragment);

    void inject(VacationResultFragment fragment);

    void inject(SpecificListingActivity activity);

    void inject(BookingDialogFragment fragment);

    void inject(BookingDialogFragment2 fragment);

    void inject(CheckOutActivity activity);

}
