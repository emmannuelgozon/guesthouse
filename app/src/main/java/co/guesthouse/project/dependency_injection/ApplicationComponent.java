package co.guesthouse.project.dependency_injection;

import android.app.Application;
import android.content.Context;

import javax.inject.Singleton;

import co.guesthouse.project.base.BaseApplication;
import dagger.Component;

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    void inject(BaseApplication application);

    Context context();

    Application application();

}
