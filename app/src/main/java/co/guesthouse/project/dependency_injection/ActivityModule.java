package co.guesthouse.project.dependency_injection;

import androidx.appcompat.app.AppCompatActivity;
import co.guesthouse.project.main.check_out_view.CheckOutMvp;
import co.guesthouse.project.main.check_out_view.CheckOutPresenter;
import co.guesthouse.project.main.listing_view.VacationResultMvp;
import co.guesthouse.project.main.listing_view.VacationResultPresenter;
import co.guesthouse.project.main.search_view.VacationHouseSearchMvp;
import co.guesthouse.project.main.search_view.VacationHouseSearchPresenter;
import co.guesthouse.project.main.specific_view.SpecificListingMvp;
import co.guesthouse.project.main.specific_view.SpecificListingPresenter;
import dagger.Module;
import dagger.Provides;

@Module
public class ActivityModule {

    private AppCompatActivity mActivity;

    public ActivityModule(AppCompatActivity activity){
        mActivity = activity;
    }

    @Provides
    VacationHouseSearchMvp.Presenter<VacationHouseSearchMvp.View> provideVacationHouseSearchPresenter(
            VacationHouseSearchPresenter<VacationHouseSearchMvp.View> presenter
    ){
        return presenter;
    }


    @Provides
    VacationResultMvp.Presenter<VacationResultMvp.View> provideVacationResultPresenter(
            VacationResultPresenter<VacationResultMvp.View> presenter
    ){
        return presenter;
    }


    @Provides
    SpecificListingMvp.Presenter<SpecificListingMvp.View> providesSpecificListingPresenter(
            SpecificListingPresenter<SpecificListingMvp.View> presenter
    ){
        return presenter;
    }


    @Provides
    CheckOutMvp.Presenter<CheckOutMvp.View> providesCheckoutPresenter(
            CheckOutPresenter<CheckOutMvp.View> presenter
    ){
        return presenter;
    }


}
