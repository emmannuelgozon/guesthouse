package co.guesthouse.project.base;

public interface BaseMvpView {

    void showLoadingDialog();

    void hideLoadingDialog();

    void showPopUpDialogWithMessage(String message);

    void hidePopupDialogWithMessage();
}
