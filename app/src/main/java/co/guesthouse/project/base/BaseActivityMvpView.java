package co.guesthouse.project.base;

public interface BaseActivityMvpView {

    void showLoadingDialog();

    void hideLoadingDialog();

    void showPopUpDialogWithMessage(String message);

    void hidePopupDialogWithMessage();
}
