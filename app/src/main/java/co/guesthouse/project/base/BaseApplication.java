package co.guesthouse.project.base;

import android.app.Application;

import co.guesthouse.project.dependency_injection.ApplicationComponent;
import co.guesthouse.project.dependency_injection.ApplicationModule;
import co.guesthouse.project.dependency_injection.DaggerApplicationComponent;

public class BaseApplication extends Application {

    private ApplicationComponent mApplicationComponent;


    @Override
    public void onCreate() {
        super.onCreate();

        mApplicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this)).build();

        mApplicationComponent.inject(this);
    }


    public ApplicationComponent getmApplicationComponent(){
        return mApplicationComponent;
    }
}
