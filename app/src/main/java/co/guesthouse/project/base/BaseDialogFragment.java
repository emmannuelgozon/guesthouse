package co.guesthouse.project.base;

import android.content.Context;

import androidx.fragment.app.DialogFragment;
import co.guesthouse.project.dependency_injection.ActivityComponent;

public abstract class BaseDialogFragment extends DialogFragment {

    private BaseActivity mActivity;

    protected abstract void initialize();


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof  BaseActivity){
            BaseActivity activity = (BaseActivity) context;
            mActivity = activity;
        }
    }


    public ActivityComponent getActivityComponent(){
        if(mActivity != null){
            return mActivity.getActivityComponent();
        }
        return null;
    }
}
