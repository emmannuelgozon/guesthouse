package co.guesthouse.project.base;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import co.guesthouse.R;
import co.guesthouse.project.dependency_injection.ActivityComponent;

public abstract class BaseFragment extends Fragment implements BaseMvpView{


    private BaseActivity mActivity;
    private Context context;

    private ProgressDialog  progressDialog;
    private AlertDialog alertDialog;

    protected abstract void initialize();


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        if(context instanceof  BaseActivity){
            BaseActivity activity = (BaseActivity) context;
            mActivity = activity;
        }
    }


    public ActivityComponent getActivityComponent(){
        if(mActivity != null){
            return mActivity.getActivityComponent();
        }
        return null;
    }

    @Override
    public void showLoadingDialog() {

        progressDialog = ProgressDialog.show(context, getResources().getString(R.string.loading) ,
                getResources().getString(R.string.please_wait), true);
    }

    @Override
    public void hideLoadingDialog() {

        if(progressDialog != null){
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    public void showPopUpDialogWithMessage(String message) {
        alertDialog = new AlertDialog.Builder(context)
                .setTitle(getResources().getString(R.string.error_dialog_title))
                .setMessage(message)
                .setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        alertDialog.dismiss();
                        alertDialog = null;
                    }
                })
                .show();
    }

    public void hidePopupDialogWithMessage() {
        if(alertDialog != null){
            alertDialog.dismiss();
            alertDialog = null;
        }
    }
}

