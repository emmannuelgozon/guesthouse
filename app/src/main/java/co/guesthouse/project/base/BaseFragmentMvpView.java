package co.guesthouse.project.base;

public interface BaseFragmentMvpView {

    void showLoadingDialog();

    void hideLoadingDialog();

    void showPopUpDialogWithMessage(String message);

    void hidePopupDialogWithMessage();
}
