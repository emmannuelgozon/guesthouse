package co.guesthouse.project.base;


import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import co.guesthouse.R;
import co.guesthouse.project.dependency_injection.ActivityComponent;
import co.guesthouse.project.dependency_injection.ActivityModule;
import co.guesthouse.project.dependency_injection.DaggerActivityComponent;

public abstract class BaseActivity extends AppCompatActivity implements BaseMvpView{

    private static final String TAG = "BaseActivity";

    protected abstract void initialize();

    private ActivityComponent mActivityComponent;
    private ProgressDialog progressDialog;
    private AlertDialog alertDialog;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivityComponent = DaggerActivityComponent.builder()
                .activityModule(new ActivityModule(this))
                .applicationComponent(((BaseApplication)getApplication()).getmApplicationComponent())
                .build();
        mActivityComponent.inject(this);
    }

    @Override
    public void showLoadingDialog() {

        Log.d(TAG, "showLoadingDialog: ");
        progressDialog = ProgressDialog.show(this, getResources().getString(R.string.loading) ,
                getResources().getString(R.string.please_wait), true);
    }

    @Override
    public void hideLoadingDialog() {

        Log.d(TAG, "hideLoadingDialog: ");
        if(progressDialog != null){
            progressDialog.dismiss();
            progressDialog = null;
        }
    }


    public void showPopUpDialogWithMessage(String message) {
        alertDialog = new AlertDialog.Builder(this)
                .setTitle(getResources().getString(R.string.error_dialog_title))
                .setMessage(message)
                .setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        alertDialog.dismiss();
                        alertDialog = null;
                    }
                })
                .show();
    }

    @Override
    public void hidePopupDialogWithMessage() {
        if(alertDialog != null){
            alertDialog.dismiss();
            alertDialog = null;
        }
    }

    public ActivityComponent getActivityComponent(){
        return mActivityComponent;
    }
}
