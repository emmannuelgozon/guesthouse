package co.guesthouse.project.base;

import javax.inject.Inject;

import co.guesthouse.project.data.ApplicationDataManager;

public class BasePresenter<V extends BaseMvpView> implements BaseMvpPresenter<V>{

    private static final String TAG = "BasePresenter";

    private final ApplicationDataManager mApplicationDataManager;

    private V mMvpView;

    @Inject
    public BasePresenter(ApplicationDataManager applicationDataManager){
        this.mApplicationDataManager = applicationDataManager;
    }

    @Override
    public void onAttach(V baseMvpView) {
        mMvpView = baseMvpView;
    }

    @Override
    public void onDettach() {
        mMvpView = null;
    }

    @Override
    public boolean isViewAttached() {
        return mMvpView != null;
    }

    @Override
    public V getMvpView() {
        return mMvpView;
    }


    public ApplicationDataManager getApplicationDataManager() {
        return mApplicationDataManager;
    }
}
