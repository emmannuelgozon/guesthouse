package co.guesthouse.project.base;

public interface BaseMvpPresenter<V extends BaseMvpView> {

    void onAttach(V baseMvpView);

    void onDettach();

    boolean isViewAttached();

    V getMvpView();

}
