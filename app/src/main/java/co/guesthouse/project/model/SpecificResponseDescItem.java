package co.guesthouse.project.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class SpecificResponseDescItem implements Parcelable {

	@SerializedName("title")
	private String title;

	@SerializedName("desc")
	private String desc;

	public void setTitle(String title){
		this.title = title;
	}

	public String getTitle(){
		return title;
	}

	public void setDesc(String desc){
		this.desc = desc;
	}

	public String getDesc(){
		return desc;
	}

	@Override
 	public String toString(){
		return 
			"SpecificResponseDescItem{" +
			"title = '" + title + '\'' + 
			",desc = '" + desc + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.title);
		dest.writeString(this.desc);
	}

	public SpecificResponseDescItem() {
	}

	protected SpecificResponseDescItem(Parcel in) {
		this.title = in.readString();
		this.desc = in.readString();
	}

	public static final Parcelable.Creator<SpecificResponseDescItem> CREATOR = new Parcelable.Creator<SpecificResponseDescItem>() {
		@Override
		public SpecificResponseDescItem createFromParcel(Parcel source) {
			return new SpecificResponseDescItem(source);
		}

		@Override
		public SpecificResponseDescItem[] newArray(int size) {
			return new SpecificResponseDescItem[size];
		}
	};
}