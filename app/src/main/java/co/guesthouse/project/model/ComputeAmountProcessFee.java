package co.guesthouse.project.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class ComputeAmountProcessFee implements Parcelable {

    @SerializedName("process_fee")
    private String processFee;

    @SerializedName("subtotal")
    private String subtotal;

    public String getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(String subtotal) {
        this.subtotal = subtotal;
    }

    public String getProcessFee() {
        return processFee;
    }

    public void setProcessFee(String processFee) {
        this.processFee = processFee;
    }



    @Override
    public String toString(){
        return
                "ComputeAmountProcessFee{" +
                        "processFee = '" + processFee + '\'' +
                        ",subtotal = '" + subtotal + '\'' +
                        "}";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.processFee);
        dest.writeString(this.subtotal);
    }

    public ComputeAmountProcessFee() {
    }

    protected ComputeAmountProcessFee(Parcel in) {
        this.processFee = in.readString();
        this.subtotal = in.readString();
    }

    public static final Parcelable.Creator<ComputeAmountProcessFee> CREATOR = new Parcelable.Creator<ComputeAmountProcessFee>() {
        @Override
        public ComputeAmountProcessFee createFromParcel(Parcel source) {
            return new ComputeAmountProcessFee(source);
        }

        @Override
        public ComputeAmountProcessFee[] newArray(int size) {
            return new ComputeAmountProcessFee[size];
        }
    };
}
