package co.guesthouse.project.model.check_reservation_dates;

import com.google.gson.annotations.SerializedName;

public class ReservationDatesResponse {

    @SerializedName("message")
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @SerializedName("status")
    private String status;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
