package co.guesthouse.project.model.checkout_confirm;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class Outcome{

	@SerializedName("reason")
	private Object reason;

	@SerializedName("risk_level")
	private String riskLevel;

	@SerializedName("risk_score")
	private int riskScore;

	@SerializedName("seller_message")
	private String sellerMessage;

	@SerializedName("network_status")
	private String networkStatus;

	@SerializedName("type")
	private String type;

	public void setReason(Object reason){
		this.reason = reason;
	}

	public Object getReason(){
		return reason;
	}

	public void setRiskLevel(String riskLevel){
		this.riskLevel = riskLevel;
	}

	public String getRiskLevel(){
		return riskLevel;
	}

	public void setRiskScore(int riskScore){
		this.riskScore = riskScore;
	}

	public int getRiskScore(){
		return riskScore;
	}

	public void setSellerMessage(String sellerMessage){
		this.sellerMessage = sellerMessage;
	}

	public String getSellerMessage(){
		return sellerMessage;
	}

	public void setNetworkStatus(String networkStatus){
		this.networkStatus = networkStatus;
	}

	public String getNetworkStatus(){
		return networkStatus;
	}

	public void setType(String type){
		this.type = type;
	}

	public String getType(){
		return type;
	}

	@Override
 	public String toString(){
		return 
			"Outcome{" + 
			"reason = '" + reason + '\'' + 
			",risk_level = '" + riskLevel + '\'' + 
			",risk_score = '" + riskScore + '\'' + 
			",seller_message = '" + sellerMessage + '\'' + 
			",network_status = '" + networkStatus + '\'' + 
			",type = '" + type + '\'' + 
			"}";
		}
}