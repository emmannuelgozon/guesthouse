package co.guesthouse.project.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;



public class SearchResponseListingItem implements Parcelable {

	@SerializedName("img")
	private String img;

	@SerializedName("rate")
	private int rate;

	@SerializedName("price")
	private int price;

	@SerializedName("guest")
	private int guest;

	@SerializedName("id")
	private int id;

	@SerializedName("bedroom")
	private int bedroom;

	@SerializedName("listing_name")
	private String listingName;

	@SerializedName("room_type")
	private String roomType;

	@SerializedName("permalink")
	private String permalink;

	protected SearchResponseListingItem(Parcel in) {
		img = in.readString();
		rate = in.readInt();
		price = in.readInt();
		guest = in.readInt();
		id = in.readInt();
		bedroom = in.readInt();
		listingName = in.readString();
		roomType = in.readString();
		permalink = in.readString();
	}

	public static final Creator<SearchResponseListingItem> CREATOR = new Creator<SearchResponseListingItem>() {
		@Override
		public SearchResponseListingItem createFromParcel(Parcel in) {
			return new SearchResponseListingItem(in);
		}

		@Override
		public SearchResponseListingItem[] newArray(int size) {
			return new SearchResponseListingItem[size];
		}
	};

	public void setImg(String img){
		this.img = img;
	}

	public String getImg(){
		return img;
	}

	public void setRate(int rate){
		this.rate = rate;
	}

	public int getRate(){
		return rate;
	}

	public void setPrice(int price){
		this.price = price;
	}

	public int getPrice(){
		return price;
	}

	public void setGuest(int guest){
		this.guest = guest;
	}

	public int getGuest(){
		return guest;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setBedroom(int bedroom){
		this.bedroom = bedroom;
	}

	public int getBedroom(){
		return bedroom;
	}

	public void setListingName(String listingName){
		this.listingName = listingName;
	}

	public String getListingName(){
		return listingName;
	}

	public void setRoomType(String roomType){
		this.roomType = roomType;
	}

	public String getRoomType(){
		return roomType;
	}

	public void setPermalink(String permalink) {
		this.permalink = permalink;
	}

	public String getPermalink() {
		return permalink;
	}




	@Override
 	public String toString(){
		return 
			"SearchResponseListingItem{" +
			"img = '" + img + '\'' + 
			",rate = '" + rate + '\'' + 
			",price = '" + price + '\'' + 
			",guest = '" + guest + '\'' + 
			",id = '" + id + '\'' + 
			",bedroom = '" + bedroom + '\'' + 
			",listing_name = '" + listingName + '\'' + 
			",room_type = '" + roomType + '\'' +
			",permalink = '" + permalink + '\'' +
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(img);
		dest.writeInt(rate);
		dest.writeInt(price);
		dest.writeInt(guest);
		dest.writeInt(id);
		dest.writeInt(bedroom);
		dest.writeString(listingName);
		dest.writeString(roomType);
		dest.writeString(permalink);
	}
}