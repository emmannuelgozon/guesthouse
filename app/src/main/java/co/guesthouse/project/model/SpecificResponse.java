package co.guesthouse.project.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class SpecificResponse implements Parcelable {

	@SerializedName("bed")
	private int bed;

	@SerializedName("amenities")
	private String amenities;

	@SerializedName("img")
	private List<String> img;

	@SerializedName("address")
	private String address;

	@SerializedName("main_image")
	private String mainImage;

	@SerializedName("bedroom")
	private int bedroom;

	@SerializedName("long")
	private String jsonMemberLong;

	@SerializedName("roomtype")
	private String roomtype;

	@SerializedName("price")
	private int price;

	@SerializedName("name")
	private String name;

	@SerializedName("guest")
	private int guest;

	@SerializedName("id")
	private int id;

	@SerializedName("bathroom")
	private int bathroom;

	@SerializedName("lat")
	private String lat;

	@SerializedName("desc")
	private List<SpecificResponseDescItem> desc;


	public void setBed(int bed){
		this.bed = bed;
	}

	public int getBed(){
		return bed;
	}

	public void setAmenities(String amenities){
		this.amenities = amenities;
	}

	public String getAmenities(){
		return amenities;
	}

	public void setImg(List<String> img){
		this.img = img;
	}

	public List<String> getImg(){
		return img;
	}

	public void setAddress(String address){
		this.address = address;
	}

	public String getAddress(){
		return address;
	}

	public void setMainImage(String mainImage){
		this.mainImage = mainImage;
	}

	public String getMainImage(){
		return mainImage;
	}

	public void setBedroom(int bedroom){
		this.bedroom = bedroom;
	}

	public int getBedroom(){
		return bedroom;
	}

	public void setJsonMemberLong(String jsonMemberLong){
		this.jsonMemberLong = jsonMemberLong;
	}

	public String getJsonMemberLong(){
		return jsonMemberLong;
	}

	public void setRoomtype(String roomtype){
		this.roomtype = roomtype;
	}

	public String getRoomtype(){
		return roomtype;
	}

	public void setPrice(int price){
		this.price = price;
	}

	public int getPrice(){
		return price;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setGuest(int guest){
		this.guest = guest;
	}

	public int getGuest(){
		return guest;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setBathroom(int bathroom){
		this.bathroom = bathroom;
	}

	public int getBathroom(){
		return bathroom;
	}

	public void setLat(String lat){
		this.lat = lat;
	}

	public String getLat(){
		return lat;
	}

	public void setDesc(List<SpecificResponseDescItem> desc){
		this.desc = desc;
	}

	public List<SpecificResponseDescItem> getDesc(){
		return desc;
	}

	@Override
 	public String toString(){
		return 
			"SpecificResponse{" +
			"bed = '" + bed + '\'' +
			",amenities = '" + amenities + '\'' +
			",img = '" + img + '\'' +
			",address = '" + address + '\'' +
			",main_image = '" + mainImage + '\'' +
			",bedroom = '" + bedroom + '\'' +
			",long = '" + jsonMemberLong + '\'' +
			",roomtype = '" + roomtype + '\'' +
			",price = '" + price + '\'' +
			",name = '" + name + '\'' +
			",guest = '" + guest + '\'' +
			",id = '" + id + '\'' +
			",bathroom = '" + bathroom + '\'' +
			",lat = '" + lat + '\'' +
			",desc = '" + desc + '\'' +
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(this.bed);
		dest.writeString(this.amenities);
		dest.writeStringList(this.img);
		dest.writeString(this.address);
		dest.writeString(this.mainImage);
		dest.writeInt(this.bedroom);
		dest.writeString(this.jsonMemberLong);
		dest.writeString(this.roomtype);
		dest.writeInt(this.price);
		dest.writeString(this.name);
		dest.writeInt(this.guest);
		dest.writeInt(this.id);
		dest.writeInt(this.bathroom);
		dest.writeString(this.lat);
		dest.writeTypedList(this.desc);
	}

	public SpecificResponse() {
	}

	protected SpecificResponse(Parcel in) {
		this.bed = in.readInt();
		this.amenities = in.readString();
		this.img = in.createStringArrayList();
		this.address = in.readString();
		this.mainImage = in.readString();
		this.bedroom = in.readInt();
		this.jsonMemberLong = in.readString();
		this.roomtype = in.readString();
		this.price = in.readInt();
		this.name = in.readString();
		this.guest = in.readInt();
		this.id = in.readInt();
		this.bathroom = in.readInt();
		this.lat = in.readString();
		this.desc = in.createTypedArrayList(SpecificResponseDescItem.CREATOR);
	}

	public static final Parcelable.Creator<SpecificResponse> CREATOR = new Parcelable.Creator<SpecificResponse>() {
		@Override
		public SpecificResponse createFromParcel(Parcel source) {
			return new SpecificResponse(source);
		}

		@Override
		public SpecificResponse[] newArray(int size) {
			return new SpecificResponse[size];
		}
	};
}