package co.guesthouse.project.model;

import android.os.Parcel;
import android.os.Parcelable;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

public class ComputeAmountItems implements Parcelable {

	@SerializedName("price")
	private int price;

	@SerializedName("nights")
	private int nights;

	@SerializedName("subtotal")
	private int subtotal;

	public void setPrice(int price){
		this.price = price;
	}

	public int getPrice(){
		return price;
	}

	public void setNights(int nights){
		this.nights = nights;
	}

	public int getNights(){
		return nights;
	}

	public void setSubtotal(int subtotal){
		this.subtotal = subtotal;
	}

	public int getSubtotal(){
		return subtotal;
	}

	@Override
 	public String toString(){
		return 
			"ComputeAmountItems{" +
			"price = '" + price + '\'' + 
			",nights = '" + nights + '\'' + 
			",subtotal = '" + subtotal + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(this.price);
		dest.writeInt(this.nights);
		dest.writeInt(this.subtotal);
	}

	public ComputeAmountItems() {
	}

	protected ComputeAmountItems(Parcel in) {
		this.price = in.readInt();
		this.nights = in.readInt();
		this.subtotal = in.readInt();
	}

	public static final Parcelable.Creator<ComputeAmountItems> CREATOR = new Parcelable.Creator<ComputeAmountItems>() {
		@Override
		public ComputeAmountItems createFromParcel(Parcel source) {
			return new ComputeAmountItems(source);
		}

		@Override
		public ComputeAmountItems[] newArray(int size) {
			return new ComputeAmountItems[size];
		}
	};
}