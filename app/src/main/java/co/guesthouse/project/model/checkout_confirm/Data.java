package co.guesthouse.project.model.checkout_confirm;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class Data{

	@SerializedName("payment_ret")
	private PaymentRet paymentRet;

	@SerializedName("status")
	private String status;

	public void setPaymentRet(PaymentRet paymentRet){
		this.paymentRet = paymentRet;
	}

	public PaymentRet getPaymentRet(){
		return paymentRet;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"Data{" + 
			"payment_ret = '" + paymentRet + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}