package co.guesthouse.project.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;


public class ComputeAmountTotal implements Parcelable {

    @SerializedName("total")
    private String total;

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    @Override
    public String toString(){
        return
                "ComputeAmountTotal{" +
                        "Total = '" + total + '\'' +
                        "}";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.total);
    }

    public ComputeAmountTotal() {
    }

    protected ComputeAmountTotal(Parcel in) {
        this.total = in.readString();
    }

    public static final Parcelable.Creator<ComputeAmountTotal> CREATOR = new Parcelable.Creator<ComputeAmountTotal>() {
        @Override
        public ComputeAmountTotal createFromParcel(Parcel source) {
            return new ComputeAmountTotal(source);
        }

        @Override
        public ComputeAmountTotal[] newArray(int size) {
            return new ComputeAmountTotal[size];
        }
    };
}
