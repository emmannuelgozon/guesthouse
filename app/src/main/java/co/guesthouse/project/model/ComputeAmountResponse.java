package co.guesthouse.project.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.SerializedName;

public class ComputeAmountResponse implements Parcelable {

	@SerializedName("items")
	private List<ComputeAmountItems> items;

	private ComputeAmountProcessFee processFee;
	private ComputeAmountTotal total;


	public void setItems(List<ComputeAmountItems> items){
		this.items = items;
	}

	public List<ComputeAmountItems> getItems(){
		return items;
	}

	public ComputeAmountProcessFee getProcessFee() {
		return processFee;
	}

	public void setProcessFee(ComputeAmountProcessFee processFee) {
		this.processFee = processFee;
	}

	public ComputeAmountTotal getTotal() {
		return total;
	}

	public void setTotal(ComputeAmountTotal total) {
		this.total = total;
	}

	@Override
 	public String toString(){
		return 
			"ComputeAmountResponse{" + 
			"items = '" + items + '\'' +
					"processFee = '" + processFee + '\'' +
					"total = '" + total + '\'' +
					"}";
		}

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(this.items);
        dest.writeParcelable(this.processFee, flags);
        dest.writeParcelable(this.total, flags);
    }

    public ComputeAmountResponse() {
    }

    protected ComputeAmountResponse(Parcel in) {
        this.items = new ArrayList<ComputeAmountItems>();
        in.readList(this.items, ComputeAmountItems.class.getClassLoader());
        this.processFee = in.readParcelable(ComputeAmountProcessFee.class.getClassLoader());
        this.total = in.readParcelable(ComputeAmountTotal.class.getClassLoader());
    }

    public static final Parcelable.Creator<ComputeAmountResponse> CREATOR = new Parcelable.Creator<ComputeAmountResponse>() {
        @Override
        public ComputeAmountResponse createFromParcel(Parcel source) {
            return new ComputeAmountResponse(source);
        }

        @Override
        public ComputeAmountResponse[] newArray(int size) {
            return new ComputeAmountResponse[size];
        }
    };
}