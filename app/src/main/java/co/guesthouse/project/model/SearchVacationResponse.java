package co.guesthouse.project.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SearchVacationResponse{

	@SerializedName("data")
	private List<SearchResponseListingItem> data;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private String status;

	public void setData(List<SearchResponseListingItem> data){
		this.data = data;
	}

	public List<SearchResponseListingItem> getData(){
		return data;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"SearchVacationResponse{" + 
			"data = '" + data + '\'' + 
			",message = '" + message + '\'' + 
			",status = '" + status + '\'' + 
			"}";
	}
}