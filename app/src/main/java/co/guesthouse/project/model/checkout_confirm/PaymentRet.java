package co.guesthouse.project.model.checkout_confirm;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class PaymentRet{

	@SerializedName("balance_transaction")
	private String balanceTransaction;

	@SerializedName("metadata")
	private List<Object> metadata;

	@SerializedName("livemode")
	private boolean livemode;

	@SerializedName("destination")
	private Object destination;

	@SerializedName("description")
	private String description;

	@SerializedName("failure_message")
	private Object failureMessage;

	@SerializedName("fraud_details")
	private List<Object> fraudDetails;

	@SerializedName("source")
	private Source source;

	@SerializedName("amount_refunded")
	private int amountRefunded;

	@SerializedName("refunds")
	private Refunds refunds;

	@SerializedName("statement_descriptor")
	private String statementDescriptor;

	@SerializedName("transfer_data")
	private Object transferData;

	@SerializedName("receipt_url")
	private String receiptUrl;

	@SerializedName("shipping")
	private Object shipping;

	@SerializedName("review")
	private Object review;

	@SerializedName("captured")
	private boolean captured;

	@SerializedName("currency")
	private String currency;

	@SerializedName("refunded")
	private boolean refunded;

	@SerializedName("id")
	private String id;

	@SerializedName("outcome")
	private Outcome outcome;

	@SerializedName("order")
	private Object order;

	@SerializedName("dispute")
	private Object dispute;

	@SerializedName("amount")
	private int amount;

	@SerializedName("failure_code")
	private Object failureCode;

	@SerializedName("transfer_group")
	private Object transferGroup;

	@SerializedName("on_behalf_of")
	private Object onBehalfOf;

	@SerializedName("created")
	private int created;

	@SerializedName("source_transfer")
	private Object sourceTransfer;

	@SerializedName("receipt_number")
	private Object receiptNumber;

	@SerializedName("application")
	private Object application;

	@SerializedName("receipt_email")
	private Object receiptEmail;

	@SerializedName("paid")
	private boolean paid;

	@SerializedName("application_fee")
	private Object applicationFee;

	@SerializedName("payment_intent")
	private Object paymentIntent;

	@SerializedName("invoice")
	private Object invoice;

	@SerializedName("application_fee_amount")
	private Object applicationFeeAmount;

	@SerializedName("object")
	private String object;

	@SerializedName("customer")
	private Object customer;

	@SerializedName("status")
	private String status;

	public void setBalanceTransaction(String balanceTransaction){
		this.balanceTransaction = balanceTransaction;
	}

	public String getBalanceTransaction(){
		return balanceTransaction;
	}

	public void setMetadata(List<Object> metadata){
		this.metadata = metadata;
	}

	public List<Object> getMetadata(){
		return metadata;
	}

	public void setLivemode(boolean livemode){
		this.livemode = livemode;
	}

	public boolean isLivemode(){
		return livemode;
	}

	public void setDestination(Object destination){
		this.destination = destination;
	}

	public Object getDestination(){
		return destination;
	}

	public void setDescription(String description){
		this.description = description;
	}

	public String getDescription(){
		return description;
	}

	public void setFailureMessage(Object failureMessage){
		this.failureMessage = failureMessage;
	}

	public Object getFailureMessage(){
		return failureMessage;
	}

	public void setFraudDetails(List<Object> fraudDetails){
		this.fraudDetails = fraudDetails;
	}

	public List<Object> getFraudDetails(){
		return fraudDetails;
	}

	public void setSource(Source source){
		this.source = source;
	}

	public Source getSource(){
		return source;
	}

	public void setAmountRefunded(int amountRefunded){
		this.amountRefunded = amountRefunded;
	}

	public int getAmountRefunded(){
		return amountRefunded;
	}

	public void setRefunds(Refunds refunds){
		this.refunds = refunds;
	}

	public Refunds getRefunds(){
		return refunds;
	}

	public void setStatementDescriptor(String statementDescriptor){
		this.statementDescriptor = statementDescriptor;
	}

	public String getStatementDescriptor(){
		return statementDescriptor;
	}

	public void setTransferData(Object transferData){
		this.transferData = transferData;
	}

	public Object getTransferData(){
		return transferData;
	}

	public void setReceiptUrl(String receiptUrl){
		this.receiptUrl = receiptUrl;
	}

	public String getReceiptUrl(){
		return receiptUrl;
	}

	public void setShipping(Object shipping){
		this.shipping = shipping;
	}

	public Object getShipping(){
		return shipping;
	}

	public void setReview(Object review){
		this.review = review;
	}

	public Object getReview(){
		return review;
	}

	public void setCaptured(boolean captured){
		this.captured = captured;
	}

	public boolean isCaptured(){
		return captured;
	}

	public void setCurrency(String currency){
		this.currency = currency;
	}

	public String getCurrency(){
		return currency;
	}

	public void setRefunded(boolean refunded){
		this.refunded = refunded;
	}

	public boolean isRefunded(){
		return refunded;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setOutcome(Outcome outcome){
		this.outcome = outcome;
	}

	public Outcome getOutcome(){
		return outcome;
	}

	public void setOrder(Object order){
		this.order = order;
	}

	public Object getOrder(){
		return order;
	}

	public void setDispute(Object dispute){
		this.dispute = dispute;
	}

	public Object getDispute(){
		return dispute;
	}

	public void setAmount(int amount){
		this.amount = amount;
	}

	public int getAmount(){
		return amount;
	}

	public void setFailureCode(Object failureCode){
		this.failureCode = failureCode;
	}

	public Object getFailureCode(){
		return failureCode;
	}

	public void setTransferGroup(Object transferGroup){
		this.transferGroup = transferGroup;
	}

	public Object getTransferGroup(){
		return transferGroup;
	}

	public void setOnBehalfOf(Object onBehalfOf){
		this.onBehalfOf = onBehalfOf;
	}

	public Object getOnBehalfOf(){
		return onBehalfOf;
	}

	public void setCreated(int created){
		this.created = created;
	}

	public int getCreated(){
		return created;
	}

	public void setSourceTransfer(Object sourceTransfer){
		this.sourceTransfer = sourceTransfer;
	}

	public Object getSourceTransfer(){
		return sourceTransfer;
	}

	public void setReceiptNumber(Object receiptNumber){
		this.receiptNumber = receiptNumber;
	}

	public Object getReceiptNumber(){
		return receiptNumber;
	}

	public void setApplication(Object application){
		this.application = application;
	}

	public Object getApplication(){
		return application;
	}

	public void setReceiptEmail(Object receiptEmail){
		this.receiptEmail = receiptEmail;
	}

	public Object getReceiptEmail(){
		return receiptEmail;
	}

	public void setPaid(boolean paid){
		this.paid = paid;
	}

	public boolean isPaid(){
		return paid;
	}

	public void setApplicationFee(Object applicationFee){
		this.applicationFee = applicationFee;
	}

	public Object getApplicationFee(){
		return applicationFee;
	}

	public void setPaymentIntent(Object paymentIntent){
		this.paymentIntent = paymentIntent;
	}

	public Object getPaymentIntent(){
		return paymentIntent;
	}

	public void setInvoice(Object invoice){
		this.invoice = invoice;
	}

	public Object getInvoice(){
		return invoice;
	}

	public void setApplicationFeeAmount(Object applicationFeeAmount){
		this.applicationFeeAmount = applicationFeeAmount;
	}

	public Object getApplicationFeeAmount(){
		return applicationFeeAmount;
	}

	public void setObject(String object){
		this.object = object;
	}

	public String getObject(){
		return object;
	}

	public void setCustomer(Object customer){
		this.customer = customer;
	}

	public Object getCustomer(){
		return customer;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"PaymentRet{" + 
			"balance_transaction = '" + balanceTransaction + '\'' + 
			",metadata = '" + metadata + '\'' + 
			",livemode = '" + livemode + '\'' + 
			",destination = '" + destination + '\'' + 
			",description = '" + description + '\'' + 
			",failure_message = '" + failureMessage + '\'' + 
			",fraud_details = '" + fraudDetails + '\'' + 
			",source = '" + source + '\'' + 
			",amount_refunded = '" + amountRefunded + '\'' + 
			",refunds = '" + refunds + '\'' + 
			",statement_descriptor = '" + statementDescriptor + '\'' + 
			",transfer_data = '" + transferData + '\'' + 
			",receipt_url = '" + receiptUrl + '\'' + 
			",shipping = '" + shipping + '\'' + 
			",review = '" + review + '\'' + 
			",captured = '" + captured + '\'' + 
			",currency = '" + currency + '\'' + 
			",refunded = '" + refunded + '\'' + 
			",id = '" + id + '\'' + 
			",outcome = '" + outcome + '\'' + 
			",order = '" + order + '\'' + 
			",dispute = '" + dispute + '\'' + 
			",amount = '" + amount + '\'' + 
			",failure_code = '" + failureCode + '\'' + 
			",transfer_group = '" + transferGroup + '\'' + 
			",on_behalf_of = '" + onBehalfOf + '\'' + 
			",created = '" + created + '\'' + 
			",source_transfer = '" + sourceTransfer + '\'' + 
			",receipt_number = '" + receiptNumber + '\'' + 
			",application = '" + application + '\'' + 
			",receipt_email = '" + receiptEmail + '\'' + 
			",paid = '" + paid + '\'' + 
			",application_fee = '" + applicationFee + '\'' + 
			",payment_intent = '" + paymentIntent + '\'' + 
			",invoice = '" + invoice + '\'' + 
			",application_fee_amount = '" + applicationFeeAmount + '\'' + 
			",object = '" + object + '\'' + 
			",customer = '" + customer + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}