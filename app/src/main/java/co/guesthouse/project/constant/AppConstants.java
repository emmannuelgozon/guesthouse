package co.guesthouse.project.constant;

public class AppConstants {


    public static final int DATEPICKER_CHECKIN_REQUEST_CODE = 9901;
    public static final int DATEPICKER_CHECKOUT_REQUEST_CODE = 9902;


    //==================================== API Response ========================================
    public static final String SEARCH_RESPONSE_SUCCESS = "success";

    //==================================== months ========================================
    public enum ENUM_MONTHS_SHORT {
            Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov, Dec
    };

    public enum ENUM_MONTH_LONG {

        JANUARY(1, "January"),
        FEBRUARY(2, "February"),
        MARCH(3, "March"),
        APRIL(4, "April"),
        MAY(5, "May"),
        JUNE(6, "June"),
        JULY(7, "July"),
        AUGUST(8, "August"),
        SEPTEMBER(9, "September"),
        OCTOBER(10, "October"),
        NOVEMBER(11, "November"),
        DECEMBER(12, "December");


        private Integer key;
        private String value;

        ENUM_MONTH_LONG(Integer key, String value){}

        public Integer getKey(){
            return key;
        }

        public String getValue(){
            return value;
        }

    }


    public enum ENUM_MONTH_LONG1 {

        JANUARY(1),
        FEBRUARY(2),
        MARCH(3),
        APRIL(4),
        MAY(5),
        JUNE(6),
        JULY(7),
        AUGUST(8),
        SEPTEMBER(9),
        OCTOBER(10),
        NOVEMBER(11),
        DECEMBER(12);

        private Integer value;

        ENUM_MONTH_LONG1(Integer value){
            this.value = value;
        }

        public Integer getValue(){
            return value;
        }

    }

//    public enum ENUM_YEAR{
//        2019, 2020, 2021, 2022, 2023, 2024, 2025, 2026, 2027, 2028, 2029
//    };

    public enum ENUM_COUNTRY{
        Singapore
    }


    //==================================== date comparison ========================================
    public static final byte NO_OPERATION = 0;
    public static final byte SUBTRACT = -1;
    public static final byte ADD = 1;
}
