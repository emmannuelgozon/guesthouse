package null;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class DataItem{

	@SerializedName("img")
	private String img;

	@SerializedName("rate")
	private int rate;

	@SerializedName("price")
	private int price;

	@SerializedName("guest")
	private int guest;

	@SerializedName("id")
	private int id;

	@SerializedName("bedroom")
	private int bedroom;

	@SerializedName("listing_name")
	private String listingName;

	@SerializedName("room_type")
	private String roomType;

	public void setImg(String img){
		this.img = img;
	}

	public String getImg(){
		return img;
	}

	public void setRate(int rate){
		this.rate = rate;
	}

	public int getRate(){
		return rate;
	}

	public void setPrice(int price){
		this.price = price;
	}

	public int getPrice(){
		return price;
	}

	public void setGuest(int guest){
		this.guest = guest;
	}

	public int getGuest(){
		return guest;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setBedroom(int bedroom){
		this.bedroom = bedroom;
	}

	public int getBedroom(){
		return bedroom;
	}

	public void setListingName(String listingName){
		this.listingName = listingName;
	}

	public String getListingName(){
		return listingName;
	}

	public void setRoomType(String roomType){
		this.roomType = roomType;
	}

	public String getRoomType(){
		return roomType;
	}

	@Override
 	public String toString(){
		return 
			"SearchResponseListingItem{" +
			"img = '" + img + '\'' + 
			",rate = '" + rate + '\'' + 
			",price = '" + price + '\'' + 
			",guest = '" + guest + '\'' + 
			",id = '" + id + '\'' + 
			",bedroom = '" + bedroom + '\'' + 
			",listing_name = '" + listingName + '\'' + 
			",room_type = '" + roomType + '\'' + 
			"}";
		}
}